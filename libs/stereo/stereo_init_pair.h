/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef STEREO_INIT_PAIR_HEADER
#define STEREO_INIT_PAIR_HEADER

#include <vector>

#include "sfm/ransac_homography.h"
#include "sfm/ransac_fundamental.h"
#include "sfm/fundamental.h"
#include "sfm/bundler_common.h"
#include "sfm/camera_pose.h"
#include "sfm/defines.h"
#include "sfm/bundler_init_pair.h"
#include "stereo/stereo_features.h"
#include "stereo/defines.h"

SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN

/**
 * Tries to find an initial viewport pair to start the reconstruction with.
 * The implemented strategy sorts all pairwise matching results by the
 * number of matches and chooses the first pair where the matches cannot
 * be explained with a homography.
 */
class InitialPair : public sfm::bundler::InitialPair
{
public:
    struct Options : public sfm::bundler::InitialPair::Options
    {
        Options (void);

    };

    /**
     * The resulting initial pair with view IDs and relative camera pose.
     * If no initial pair could be found, both view IDs are set to -1.
     */
    struct Result
    {
        int view_id;
        CameraPose view_pose;
    };

public:
    explicit InitialPair (Options const& options);
    /** Initializes the module with viewport and track information. */
    void initialize (StereoViewportList const& viewports, TrackList const& tracks,StereoPairwiseMatching const& pairwise_matching);
    /** Finds a suitable initial pair and reconstructs the pose. */
    void compute_pair (Result* result);
    /** Reconstructs the pose for a given intitial pair. */
    void compute_pair (int view_id, Result* result);

private:
    struct CandidatePair
    {
        int view_id;
        Correspondences2D2D matches;
        bool operator< (CandidatePair const& other) const;
    };
    typedef std::vector<CandidatePair> CandidatePairs;

private:

    bool compute_pose (CandidatePair const& candidate,
        CameraPose* pose);
    void compute_candidate_pairs (CandidatePairs* candidates);

    void debug_output (CandidatePair const& candidate,
        std::size_t num_inliers = 0, double angle = 0.0);

private:
    Options opts;
    StereoViewportList const* viewports;
    TrackList const* tracks;
    StereoPairwiseMatching const *pairwise_matching;
};

/* ------------------------ Implementation ------------------------ */

inline
InitialPair::Options::Options (void)
{
}

inline
InitialPair::InitialPair (Options const& options)
    : sfm::bundler::InitialPair(options),opts(options)
    , viewports(NULL)
    , tracks(NULL)
{
}

inline void
InitialPair::initialize (StereoViewportList const& viewports, TrackList const& tracks,StereoPairwiseMatching const& pairwise_matching)
{
    this->viewports = &viewports;
    this->tracks = &tracks;
    this->pairwise_matching = &pairwise_matching;

}

inline bool
InitialPair::CandidatePair::operator< (CandidatePair const& other) const
{
    return this->matches.size() < other.matches.size();
}

STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END

#endif /* STEREO_INIT_PAIR_HEADER */
