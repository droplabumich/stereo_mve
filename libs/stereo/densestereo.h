/*
 * Copyright (C) 2015, Ronny Klowsky, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef DENSESTEREO_H
#define DENSESTEREO_H

#include <fstream>
#include <string>
#include <vector>
#include <queue>

#include "mve/bundle.h"
#include "mve/image.h"
#include "mve/scene.h"
#include "dmrecon/defines.h"
#include "dmrecon/patch_optimization.h"
#include "dmrecon/single_view.h"
#include "dmrecon/progress.h"
#include "stereo/stereo_common.h"
#include "stereo/single_view_stereo.h"
MVS_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
struct QueueData
{
    int x;
    int y;
    float confidence;
    float depth;
    float dz_i, dz_j;
    IndexSet localViewIDs;

    bool operator< (const QueueData& rhs) const;
};

class DenseStereo
{
public:
    DenseStereo(mve::Scene::Ptr scene, Settings const& settings);

    std::size_t getRefViewNr() const;
    Progress const& getProgress() const;
    Progress& getProgress();
    void start();

private:
    mve::Scene::Ptr scene;
    mve::Bundle::ConstPtr bundle;
    std::vector<mvs::stereo::SingleViewStereo::Ptr> views;

    Settings settings;
    std::priority_queue<QueueData> prQueue;
    IndexSet neighViews;
    std::vector<SingleView::Ptr> imgNeighbors;
    int width;
    int height;
    Progress progress;
     Stereo_Calib_CV stereo_calibration;

    void doStereo();
    /*void globalViewSelection();
    void processFeatures();
    void processQueue();
    void refillQueueFromLowRes();*/
};

/* ------------------------- Implementation ----------------------- */

inline bool
QueueData::operator< (const QueueData& rhs) const
{
    return (confidence < rhs.confidence);
}

inline const Progress&
DenseStereo::getProgress() const
{
    return progress;
}

inline Progress&
DenseStereo::getProgress()
{
    return progress;
}

inline std::size_t
DenseStereo::getRefViewNr() const
{
    return settings.refViewNr;
}
STEREO_NAMESPACE_END
MVS_NAMESPACE_END

#endif
