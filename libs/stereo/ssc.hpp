#pragma once

#include "so3.hpp"
#include "simple_linalg.hpp"



namespace math {

namespace ssc {

static const size_t DOF_X = 0;
static const size_t DOF_Y = 1;
static const size_t DOF_Z = 2;
static const size_t DOF_R = 3;
static const size_t DOF_P = 4;
static const size_t DOF_H = 5;

static const size_t DOF_XYZ = 0;
static const size_t DOF_RPH = 3;

static const size_t XYZ_X = 0; // x,y,z indexing within xyz subvector;
static const size_t XYZ_Y = 1;
static const size_t XYZ_Z = 2;

static const size_t RPH_R = 0; // r,p,h indexing within rph subvector
static const size_t RPH_P = 1;
static const size_t RPH_H = 2;

/* computes transformation of points in frame i to points in frame j */
template <typename T>
void
H_ji (const T* x_ij, T* R, T* t)
{
    T rph[3] = {x_ij[DOF_R],
                x_ij[DOF_P],
                x_ij[DOF_H]};

    /* rotation */
    T R_T[9];
    so3::rotxyz (rph, R_T);
    simple_linalg::matrix_transpose_3x3 (R_T, R);
    simple_linalg::matrix_times_vector_3x3 (R, x_ij, t);

    /* translation */
    simple_linalg::vector_negate_3x1 (t);
}

/* computes transformation of points in frame j to points in frame i */
template <typename T>
void
H_ij (const T* x_ij, T* R, T* t)
{
    T rph[3] = {x_ij[DOF_R],
                x_ij[DOF_P],
                x_ij[DOF_H]};

    /* rotation */
    so3::rotxyz (rph, R);

    /* translation */
    t[DOF_X] = x_ij[DOF_X];
    t[DOF_Y] = x_ij[DOF_Y];
    t[DOF_Z] = x_ij[DOF_Z];
}

template <typename T>
void
transform_point_from_j_to_i (const T* x_ij, const T* point_j, T* point_i, T* R=NULL, T* t=NULL)
{
    if (!R || !t) {
        T Rbuf[9];
        T tbuf[3];
        H_ij (x_ij, Rbuf, tbuf);
        simple_linalg::matrix_times_vector_3x3 (Rbuf, point_j, point_i);
        simple_linalg::vector_plus_equals_3x1 (point_i, tbuf);
    } else {
        H_ij (x_ij, R, t);
        simple_linalg::matrix_times_vector_3x3 (R, point_j, point_i);
        simple_linalg::vector_plus_equals_3x1 (point_i, t);
    }
}

template <typename T>
void
transform_point_from_i_to_j (const T* x_ij, const T* point_i, T* point_j, T* R=NULL, T* t=NULL)
{
    if (!R || !t) {
        T Rbuf[9];
        T tbuf[3];
        H_ji (x_ij, Rbuf, tbuf);
        simple_linalg::matrix_times_vector_3x3 (Rbuf, point_i, point_j);
        simple_linalg::vector_plus_equals_3x1 (point_j, tbuf);
    } else {
        H_ji (x_ij, R, t);
        simple_linalg::matrix_times_vector_3x3 (R, point_i, point_j);
        simple_linalg::vector_plus_equals_3x1 (point_j, t);
    }
}

template <typename T>
void
head2tail (const T* x_ij, const T* x_jk, T* x_ik)
{
    T R_ij[9];
    T t_ij[3];
    H_ij (x_ij, R_ij, t_ij);

    T R_jk[9];
    T t_jk[3];
    H_ij (x_jk, R_jk, t_jk);

    T R_ik[9];
    simple_linalg::matrix_times_matrix_3x3 (R_ij, R_jk, R_ik);

    /* t_ik = R_ij*x_jk[0:3] + x_ij[0:3]     */
    T* t_ik = x_ik;
    simple_linalg::matrix_times_vector_3x3 (R_ij, x_jk, t_ik);

    /* t_ik = t_ik + x_ij[0:3] */
    simple_linalg::vector_plus_equals_3x1 (t_ik, x_ij);

    so3::rot2rph (R_ik, x_ik + DOF_RPH);
}

template <typename T>
void
inverse (const T* x_ij, T* x_ji)
{
    T R_ji[9];
    T t_ji[3];
    H_ji (x_ij, R_ji, t_ji);

    x_ji[DOF_X] = t_ji[DOF_X];
    x_ji[DOF_Y] = t_ji[DOF_Y];
    x_ji[DOF_Z] = t_ji[DOF_Z];

    so3::rot2rph (R_ji, x_ji+3);
}

template <typename T>
void
tail2tail (const T* x_ij, const T* x_ik, T* x_jk)
{
    T x_ji[6];
    inverse (x_ij, x_ji);
    head2tail (x_ji, x_ik, x_jk);
}

}

}

