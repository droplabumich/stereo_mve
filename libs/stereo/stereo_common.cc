/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <iostream>
#include <algorithm>
#include <fstream>
#include <limits>
#include <sstream>
#include <vector>
#include <cstring>
#include <cerrno>

#include "util/exception.h"
#include "stereo/stereo_common.h"
#include "stereo/stereo_matching.h"
#define PREBUNDLE_SLAM_SIGNATURE "MVE_STEREO_PREBUNDLE\n"
#define PREBUNDLE_SLAM_SIGNATURE_LEN 21

SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN



/* ------------------ Input/Output for Prebundle ------------------ */

void
save_prebundle_data (StereoViewportList const& viewports,
    StereoPairwiseMatching const& matching, std::ostream& out)
{
    /* Write signature. */
    out.write(PREBUNDLE_SLAM_SIGNATURE, PREBUNDLE_SLAM_SIGNATURE_LEN);

    /* Write number of viewports. */
    int32_t num_viewports = static_cast<int32_t>(viewports.size());
    out.write(reinterpret_cast<char const*>(&num_viewports), sizeof(int32_t));

    /* Write per-viewport data. */
    for (std::size_t i = 0; i < viewports.size(); ++i)
    {
        std::ostringstream strm;
        viewports[i].stereo_calibration.save_stereo_calibration(strm);
        int32_t calib_size=strm.str().size();
        out.write(reinterpret_cast<char const*>(&calib_size), sizeof(int32_t));
        out.write(reinterpret_cast<char const*>(&strm.str()[0]), sizeof(char)*calib_size);

        FeatureSet const& vpf_m = viewports[i].features;

        /* Write positions. */
        int32_t num_positions = static_cast<int32_t>(vpf_m.positions.size());
        out.write(reinterpret_cast<char const*>(&num_positions), sizeof(int32_t));
        for (std::size_t j = 0; j < vpf_m.positions.size(); ++j){
            out.write(reinterpret_cast<char const*>(&vpf_m.positions[j]), sizeof(math::Vec2f));
        }

        FeatureSet const& vpf_s = viewports[i].features_slave;
        int32_t num_positions_slave = static_cast<int32_t>(vpf_s.positions.size());
        out.write(reinterpret_cast<char const*>(&num_positions_slave), sizeof(int32_t));
        for (std::size_t j = 0; j < vpf_s.positions.size(); ++j){
            out.write(reinterpret_cast<char const*>(&vpf_s.positions[j]), sizeof(math::Vec2f));
        }

        std::vector<int> const& match_m_s = viewports[i].matches_master_slave;
        int32_t num_match_m_s = static_cast<int32_t>(match_m_s.size());
        out.write(reinterpret_cast<char const*>(&num_match_m_s), sizeof(int32_t));
        for (std::size_t j = 0; j < match_m_s.size(); ++j){
            out.write(reinterpret_cast<char const*>(&match_m_s[j]), sizeof(int));
        }

        /* Write colors. */
        int32_t num_colors = static_cast<int32_t>(vpf_m.colors.size());
        out.write(reinterpret_cast<char const*>(&num_colors), sizeof(int32_t));
        for (std::size_t j = 0; j < vpf_m.colors.size(); ++j)
            out.write(reinterpret_cast<char const*>(&vpf_m.colors[j]), sizeof(math::Vec3uc));

	int32_t num_colors_slave = static_cast<int32_t>(vpf_s.colors.size());
        out.write(reinterpret_cast<char const*>(&num_colors_slave), sizeof(int32_t));
        for (std::size_t j = 0; j < vpf_s.colors.size(); ++j)
            out.write(reinterpret_cast<char const*>(&vpf_s.colors[j]), sizeof(math::Vec3uc));

        int32_t num_pts_3d = static_cast<int32_t>(viewports[i].pts_3d.size());
        out.write(reinterpret_cast<char const*>(&num_pts_3d), sizeof(int32_t));
        for (std::size_t j = 0; j <viewports[i].pts_3d.size(); ++j){
            float tmp_pt[3];
            tmp_pt[0]=viewports[i].pts_3d[j][0];
            tmp_pt[1]=viewports[i].pts_3d[j][1];
            tmp_pt[2]=viewports[i].pts_3d[j][2];
            out.write(reinterpret_cast<char const*>(&tmp_pt[0]), sizeof(float)*3);
        }

    }

    /* Write number of matching pairs. */
    int32_t num_pairs = static_cast<int32_t>(matching.tvms.size());
    out.write(reinterpret_cast<char const*>(&num_pairs), sizeof(int32_t));

    /* Write per-matching pair data. */
    for (std::size_t i = 0; i < matching.tvms.size(); ++i)
    {
        StereoTwoViewMatching const& tvr = matching.tvms[i];
        int32_t id1 = static_cast<int32_t>(tvr.view_1_id);
        int32_t id2 = static_cast<int32_t>(tvr.view_2_id);
        int32_t num_matches = static_cast<int32_t>(tvr.matches.size());
        out.write(reinterpret_cast<char const*>(&id1), sizeof(int32_t));
        out.write(reinterpret_cast<char const*>(&id2), sizeof(int32_t));

        out.write(reinterpret_cast<char const*>(&(*tvr.relpose.begin())), sizeof(double)*3*4);

        out.write(reinterpret_cast<char const*>(&num_matches), sizeof(int32_t));
        for (std::size_t j = 0; j < tvr.matches.size(); ++j)
        {
            CorrespondenceIndex const& c = tvr.matches[j];
            int32_t i1 = static_cast<int32_t>(c.first);
            int32_t i2 = static_cast<int32_t>(c.second);
            out.write(reinterpret_cast<char const*>(&i1), sizeof(int32_t));
            out.write(reinterpret_cast<char const*>(&i2), sizeof(int32_t));
        }
    }
}

void
load_prebundle_data (std::istream& in, StereoViewportList* viewports,
    StereoPairwiseMatching* matching)
{
    /* Read and check file signature. */
    char signature[PREBUNDLE_SLAM_SIGNATURE_LEN + 1];
    in.read(signature, PREBUNDLE_SLAM_SIGNATURE_LEN);
    signature[PREBUNDLE_SLAM_SIGNATURE_LEN] = '\0';
    if (std::string(PREBUNDLE_SLAM_SIGNATURE) != signature)
        throw std::invalid_argument("Invalid prebundle file signature");

    viewports->clear();
    matching->tvms.clear();
    matching->view_ids2_tvms_idx.clear();

    /* Read number of viewports. */
    int32_t num_viewports;
    in.read(reinterpret_cast<char*>(&num_viewports), sizeof(int32_t));
    viewports->resize(num_viewports);

    /* Read per-viewport data. */
    for (int i = 0; i < num_viewports; ++i)
    {

        int32_t calib_size;
        in.read(reinterpret_cast<char*>(&calib_size), sizeof(int32_t));

        std::vector<char> calibfile_buf(calib_size);
        in.read(reinterpret_cast<char*>(&calibfile_buf[0]), sizeof(char)*calib_size);
        std::istringstream strm(std::string(calibfile_buf.begin(),calibfile_buf.end()));

        viewports->at(i).stereo_calibration.load_from_file(strm);

        viewports->at(i).pose.K=viewports->at(i).stereo_calibration.leftCamRect->k_matrix;
        viewports->at(i).focal_length=viewports->at(i).stereo_calibration.leftCamRect->fx();
        FeatureSet& vpf_m = viewports->at(i).features;

        /* Read positions. */
        int32_t num_positions;
        in.read(reinterpret_cast<char*>(&num_positions), sizeof(int32_t));
        vpf_m.positions.resize(num_positions);

        for (int j = 0; j < num_positions; ++j){
            in.read(reinterpret_cast<char*>(&vpf_m.positions[j]), sizeof(math::Vec2f));
        }

        in.read(reinterpret_cast<char*>(&num_positions), sizeof(int32_t));
        FeatureSet& vpf_s = viewports->at(i).features_slave;

        vpf_s.positions.resize(num_positions);

        for (int j = 0; j < num_positions; ++j){

            in.read(reinterpret_cast<char*>(&vpf_s.positions[j]), sizeof(math::Vec2f));
        }

        int32_t num_matches_m_s;
        in.read(reinterpret_cast<char*>(&num_matches_m_s), sizeof(int32_t));


        viewports->at(i).matches_master_slave.resize(num_matches_m_s);

        for (int j = 0; j < num_matches_m_s; ++j){

            in.read(reinterpret_cast<char*>(&viewports->at(i).matches_master_slave[j]), sizeof(int));
        }

        /* Read colors. */
        int32_t num_colors;
        in.read(reinterpret_cast<char*>(&num_colors), sizeof(int32_t));
        vpf_m.colors.resize(num_colors);
        for (int j = 0; j < num_colors; ++j)
            in.read(reinterpret_cast<char*>(&vpf_m.colors[j]), sizeof(math::Vec3uc));

	int32_t num_colors_slave;
        in.read(reinterpret_cast<char*>(&num_colors_slave), sizeof(int32_t));
        vpf_s.colors.resize(num_colors_slave);
        for (int j = 0; j < num_colors_slave; ++j)
            in.read(reinterpret_cast<char*>(&vpf_s.colors[j]), sizeof(math::Vec3uc));

     
        int32_t num_pts_3d;
        in.read(reinterpret_cast<char*>(&num_pts_3d), sizeof(int32_t));
        viewports->at(i).pts_3d.resize(num_pts_3d);

        for (int j = 0; j < num_pts_3d; ++j){
            float tmp_pt[3];
            in.read(reinterpret_cast<char*>(&tmp_pt[0]), sizeof(float)*3);
            viewports->at(i).pts_3d[j]=math::Vec3f(tmp_pt[0],tmp_pt[1],tmp_pt[2]);
        }

    }

    /* Read number of matching pairs. */
    int32_t num_pairs;
    in.read(reinterpret_cast<char*>(&num_pairs), sizeof(int32_t));

    /* Read per-matching pair data. */
    for (int32_t i = 0; i < num_pairs; ++i)
    {
        int32_t id1, id2, num_matches;
        math::Matrix<double,3,4> relpose;
        in.read(reinterpret_cast<char*>(&id1), sizeof(int32_t));
        in.read(reinterpret_cast<char*>(&id2), sizeof(int32_t));
        in.read(reinterpret_cast<char*>(&(*relpose.begin())), sizeof(double)*3*4);

        in.read(reinterpret_cast<char*>(&num_matches), sizeof(int32_t));

        StereoTwoViewMatching tvr;
        tvr.view_1_id = static_cast<int>(id1);
        tvr.view_2_id = static_cast<int>(id2);
        tvr.matches.reserve(num_matches);
        tvr.relpose = relpose;
        //sstd::cout << "RelPose :" << id1<< "-"<< id2<<"\n";
        //std::cout << relpose << std::endl;
        for (int32_t j = 0; j < num_matches; ++j)
        {
            int32_t i1, i2;
            in.read(reinterpret_cast<char*>(&i1), sizeof(int32_t));
            in.read(reinterpret_cast<char*>(&i2), sizeof(int32_t));
            CorrespondenceIndex c;
            c.first = static_cast<int>(i1);
            c.second = static_cast<int>(i2);
            tvr.matches.push_back(c);
        }
        matching->view_ids2_tvms_idx[std::make_pair(id1,id2)]=matching->tvms.size();
        matching->tvms.push_back(tvr);
    }
}

void
save_prebundle_to_file (StereoViewportList const& viewports,
    StereoPairwiseMatching const& matching, std::string const& filename)
{
    std::ofstream out(filename.c_str());
    if (!out.good())
        throw util::FileException(filename, std::strerror(errno));
    save_prebundle_data(viewports, matching, out);
    out.close();
}

void
load_prebundle_from_file (std::string const& filename,
    StereoViewportList* viewports, StereoPairwiseMatching* matching)
{
    std::ifstream in(filename.c_str());
    if (!in.good())
        throw util::FileException(filename, std::strerror(errno));
    try
    {
        load_prebundle_data(in, viewports, matching);
    }
    catch (...)
    {
        in.close();
        throw;
    }

    if (in.eof())
    {
        in.close();
        throw util::Exception("Premature EOF");
    }
    in.close();
}


STEREO_NAMESPACE_END
SFM_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
