/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef STEREO_FEATURES_HEADER
#define STEREO_FEATURES_HEADER

#include <string>
#include <limits>

#include "mve/scene.h"
#include "sfm/feature_set.h"
#include "sfm/bundler_common.h"
#include "sfm/defines.h"
#include "stereo/defines.h"
#include "sfm/bundler_features.h"
#include "stereo/stereo_common.h"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/LU>
#include "stereo/calib.h"
#include "stereo/ssc.hpp"
#include "stereo/costfunc.h"
#include "stereo/featureset_extended.h"
SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN

struct StereoCorrespondence2D3D :Correspondence2D3D {
    double p2d_slave[2];
};

struct StereoMatchCorrespondence2D3D {
    StereoCorrespondence2D3D sc1;
    StereoCorrespondence2D3D sc2;
};
std::ostream& operator << (std::ostream &o, const StereoCorrespondence2D3D &a);
std::ostream& operator << (std::ostream &o, const StereoMatchCorrespondence2D3D &a);
typedef std::vector<StereoMatchCorrespondence2D3D> StereoMatchCorrespondences2D3D;
void print_camera_position(sfm::CameraPose pose);

class StereoFeatures : public Features
{


public:
    struct Options : public Features::Options
    {
        Options(void);
        std::string image_embedding_slave;
        double max_epipolar_distance;
        FeatureSetExtended::Options feature_options;

    };

    explicit StereoFeatures (Options const& options);

    /** Computes features for all images in the scene. */
    void compute (mve::Scene::Ptr scene, StereoViewportList* viewports);
    void master_slave_match (StereoViewport &viewport, std::vector<int>* matches_master_slave);
    int triangulate_matches (StereoViewport &viewport, std::vector<int>* matches_master_slave);

private:
    Options opts;
};

/* ------------------------ Implementation ------------------------ */


inline
StereoFeatures::StereoFeatures (Options const& options)
    : Features(options),opts(options)
{
    opts.feature_options.max_epipolar_distance=options.max_epipolar_distance;

}

inline
StereoFeatures::Options::Options (void)
    : image_embedding_slave("slave"), max_epipolar_distance(2.0)
{
}

inline math::Vec3d cam_pt_to_rect_xyd (math::Matrix<double, 3, 3> const& k_matrix,double tx,
                                math::Vec3d cam_coord){
    double xl = k_matrix[0] * cam_coord[0] + k_matrix[2] * cam_coord[2];
    double y = k_matrix[4] * cam_coord[1] + k_matrix[5] * cam_coord[2];
    double w = cam_coord[2];
    double xd = k_matrix[0] * tx;
    double rw = 1.0/w;
    double y_rw = y * rw;
    return math::Vec3d(xl*rw, y_rw, xd*rw);
}
inline math::Vec3d pix_uvd_to_xyz (math::Matrix<double, 3, 3> const& k_matrix,double tx,math::Vec3d pix_coord){

    double x = pix_coord[0] - k_matrix[2];
    double y = pix_coord[1] - k_matrix[5];
    double z = k_matrix[0];
    double w = pix_coord[2]/tx;
    return math::Vec3d(x/w, y/w, z/w);
}
inline void get_6dof_pose(CameraPose  const &pose,double *camera_pose){
    double ea[3];
    math::Matrix<double,3,4> camera2world=math::matrix_invert_trans(pose.R.hstack(pose.t).vstack(math::Vec4d(0,0,0,1.0))).delete_row(3);
    math::so3::rot2rph(camera2world.delete_col(3).transposed().begin(),ea);
    camera_pose[0]=camera2world.col(3)[0];
    camera_pose[1]=camera2world.col(3)[1];
    camera_pose[2]=camera2world.col(3)[2];
    camera_pose[3]=ea[0];
    camera_pose[4]=ea[1];
    camera_pose[5]=ea[2];

}


inline void get_cam_pose(const double *camera_pose,CameraPose &pose){

   math::ssc::H_ji(camera_pose,pose.R.begin(),pose.t.begin());
   pose.R.transpose();
}
inline double calc_square_reproj_error(StereoViewport const& viewport,int feature_id,math::Vec3d const &pos3d){

    CameraPose  const &pose = viewport.pose;
    math::Vec2f const& pos2d = viewport.features.positions[feature_id];
    math::Vec2f const& pos2d_slave = viewport.features_slave.positions[viewport.matches_master_slave[feature_id]];
    math::Vec3d pos2d_d(pos2d[0],pos2d[1],pos2d_slave[0]-pos2d[0]);

    double camera_global[6];
    get_6dof_pose(pose,camera_global);

    double point_global[3]={pos3d[0],pos3d[1],pos3d[2]};
    double point_camera[3];

    math::ssc::transform_point_from_i_to_j (camera_global, point_global, point_camera);
    math::Vec3d local_point(point_camera[0],point_camera[1],point_camera[2]);

    math::Vec4d pos3d_4(pos3d[0],pos3d[1],pos3d[2],1.0);

    math::Vec3d local_point2=(pose.R.hstack(pose.t).vstack(math::Vec4d(0,0,0,1.0))).delete_row(3)*pos3d_4;
  //  math::Vec3d local_point2=math::matrix_invert_trans(pose.R.hstack(pose.t).vstack(math::Vec4d(0,0,0,1.0))).delete_row(3)*pos3d_4;
    //std::cout <<"FISH: "<< (local_point-local_point2).square_norm() <<std::endl;
    math::Vec3d xyd = cam_pt_to_rect_xyd(pose.K,viewport.stereo_calibration.T_mat(0),local_point2);//(x[0] / x[2], x[1] / x[2]);
   /* double uv[4];
    ceresutil::stereo_camera_projection(camera_global,point_global,viewport.stereo_calibration,uv);
    math::Vec3d altxyd(uv[0],uv[1],uv[2]-uv[0]);*/
    //std::cout << xyd <<" "<< pos2d<<"\n" << world2cam<<std::endl ;
  //  std::cout << "Presi "<<  (altxyd - xyd).square_norm()<<std::endl;
    double error= (pos2d_d - xyd).square_norm();
    return error;
}

STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END

#endif /* STEREO_FEATURES_HEADER */
