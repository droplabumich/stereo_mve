#pragma once

/* DO NOT USE for general-purpose matrix-vector products.  This package is only for small
 * matrices, in order to make the ssc.hpp code more readable.  Note that all matrices are
 * assumed *column-major*, which is the default for Eigen */


    
namespace math {

namespace simple_linalg {

template <typename T>
T
dot_3x1 (const T* a, const T* b)
{
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

template <typename T>
T
norm_3x1 (const T* x)
{
    return sqrt (x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
}

template <typename T>
void
matrix_times_vector_3x3 (const T* A, const T* x, T* Ax)
{
    Ax[0] = (A[0]*x[0]) + (A[3]*x[1]) + (A[6]*x[2]);
    Ax[1] = (A[1]*x[0]) + (A[4]*x[1]) + (A[7]*x[2]);
    Ax[2] = (A[2]*x[0]) + (A[5]*x[1]) + (A[8]*x[2]);
}

template <typename T>
void
vector_plus_equals_3x1 (T* v1, const T* v2)
{
    v1[0] += v2[0];
    v1[1] += v2[1];
    v1[2] += v2[2];
}

template <typename T>
void
vector_negate_3x1 (T* v)
{
    v[0] = -v[0];
    v[1] = -v[1];
    v[2] = -v[2];
}

template <typename T>
void
cross_product (const T* u, const T* v, T u_cross_v[3])
{
    u_cross_v[0] = u[1]*v[2] - u[2]*v[1];
    u_cross_v[1] = u[2]*v[0] - u[0]*v[2];
    u_cross_v[2] = u[2]*v[3] - u[3]*v[2];
}

template <typename T>
void
matrix_transpose_3x3 (const T* R, T* R_T)
{
    R_T[0] = R[0];
    R_T[3] = R[1];
    R_T[6] = R[2];
    R_T[1] = R[3];
    R_T[4] = R[4];
    R_T[7] = R[5];
    R_T[2] = R[6];
    R_T[5] = R[7];
    R_T[8] = R[8];
}

template <typename T>
void
matrix_times_matrix_3x3 (const T* A, const T* B, T* AB)
{
#define A(i,j) (A[j*3+i])
#define B(i,j) (B[j*3+i])
#define AB(i,j) (AB[j*3+i])

    AB(0,0) = A(0,0)*B(0,0) + A(0,1)*B(1,0) + A(0,2)*B(2,0);
    AB(1,0) = A(1,0)*B(0,0) + A(1,1)*B(1,0) + A(1,2)*B(2,0);
    AB(2,0) = A(2,0)*B(0,0) + A(2,1)*B(1,0) + A(2,2)*B(2,0);

    AB(0,1) = A(0,0)*B(0,1) + A(0,1)*B(1,1) + A(0,2)*B(2,1);
    AB(1,1) = A(1,0)*B(0,1) + A(1,1)*B(1,1) + A(1,2)*B(2,1);
    AB(2,1) = A(2,0)*B(0,1) + A(2,1)*B(1,1) + A(2,2)*B(2,1);

    AB(0,2) = A(0,0)*B(0,2) + A(0,1)*B(1,2) + A(0,2)*B(2,2);
    AB(1,2) = A(1,0)*B(0,2) + A(1,1)*B(1,2) + A(1,2)*B(2,2);
    AB(2,2) = A(2,0)*B(0,2) + A(2,1)*B(1,2) + A(2,2)*B(2,2);

#undef A
#undef B
#undef AB
}

template <typename T>
void
matrix_inverse_3x3 (const T* A, T* invA)
{
#define A(i,j) (A[j*3+i])
#define invA(i,j) (invA[j*3+i])
    T determinant =  (+A(0,0)*(A(1,1)*A(2,2)-A(2,1)*A(1,2))
                          -A(0,1)*(A(1,0)*A(2,2)-A(1,2)*A(2,0))
                          +A(0,2)*(A(1,0)*A(2,1)-A(1,1)*A(2,0)));
    T invdet = 1/determinant;
    invA(0,0) =  (A(1,1)*A(2,2)-A(2,1)*A(1,2))*invdet;
    invA(0,1) = -(A(0,1)*A(2,2)-A(0,2)*A(2,1))*invdet;
    invA(0,2) =  (A(0,1)*A(1,2)-A(0,2)*A(1,1))*invdet;
    invA(1,0) = -(A(1,0)*A(2,2)-A(1,2)*A(2,0))*invdet;
    invA(1,1) =  (A(0,0)*A(2,2)-A(0,2)*A(2,0))*invdet;
    invA(1,2) = -(A(0,0)*A(1,2)-A(1,0)*A(0,2))*invdet;
    invA(2,0) =  (A(1,0)*A(2,1)-A(2,0)*A(1,1))*invdet;
    invA(2,1) = -(A(0,0)*A(2,1)-A(2,0)*A(0,1))*invdet;
    invA(2,2) =  (A(0,0)*A(1,1)-A(1,0)*A(0,1))*invdet;
#undef A
#undef invA
}

}

}


