/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef STEREO_DEFINES_HEADER
#define STEREO_DEFINES_HEADER

#define STEREO_NAMESPACE_BEGIN namespace stereo {
#define STEREO_NAMESPACE_END }

#ifndef STD_NAMESPACE_BEGIN
#   define STD_NAMESPACE_BEGIN namespace std {
#   define STD_NAMESPACE_END }
#endif

/** Structure-from-Motion library. */
STEREO_NAMESPACE_BEGIN
STEREO_NAMESPACE_END

#endif /* MVE_DEFINES_HEADER */
