#ifndef SINGLEVIEWSTEREO_H
#define SINGLEVIEWSTEREO_H

#include "dmrecon/single_view.h"
#include "stereo/stereo_common.h"
MVS_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
class SingleViewStereo
{
public:
    typedef std::shared_ptr<SingleViewStereo> Ptr;
    typedef std::shared_ptr<SingleViewStereo const> ConstPtr;

public:
    static Ptr create (mve::Scene::Ptr scene, mve::View::Ptr view,
        std::string const& embedding,std::string const& embedding_slave);

public:
    SingleViewStereo(mve::Scene::Ptr _scene, mve::View::Ptr _view,
                                 std::string const& _embedding,std::string const& _embedding_slave) :scene(_scene)
      , view(_view)
      , embedding(_embedding), embedding_slave(_embedding_slave){sv = mvs::SingleView::create(_scene,_view,_embedding);}
    void loadColorImage(int _minLevel);
    void prepareMasterView(int scale);

    ImagePyramid::ConstPtr img_pyramid_slave;
    mvs::SingleView::Ptr sv;
    mve::Scene::Ptr scene;
    mve::View::Ptr view;
    std::string embedding;
    std::string embedding_slave;

    bool has_target_level_slave;


    mve::ByteImage::ConstPtr const& getScaledImgSlave() const;

    ImagePyramidLevel target_level_slave;


};
inline SingleViewStereo::Ptr
SingleViewStereo::create (mve::Scene::Ptr scene, mve::View::Ptr view,
    std::string const& embedding,std::string const& embedding_slave)
{
    return Ptr(new SingleViewStereo(scene, view, embedding,embedding_slave));
}
inline mve::ByteImage::ConstPtr const&
SingleViewStereo::getScaledImgSlave() const
{
    return this->target_level_slave.image;
}

STEREO_NAMESPACE_END
MVS_NAMESPACE_END

#endif // SINGLEVIEWSTEREO_H
