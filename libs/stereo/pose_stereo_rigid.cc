/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <complex>
#include <algorithm>

#include "math/matrix.h"
#include "math/vector.h"
#include "stereo/pose_stereo_rigid.h"
#include <Eigen/Core>
#include <Eigen/SVD>
#include <Eigen/LU>
#include <iostream>
using Eigen::Vector3d;
using Eigen::Matrix3d;
using namespace std;
STEREO_NAMESPACE_BEGIN
void
pose_stereo_rigid (
        math::Vec3d v1p1, math::Vec3d v1p2, math::Vec3d v1p3,
        math::Vec3d v2p1, math::Vec3d v2p2, math::Vec3d v2p3,
        math::Matrix<double, 3, 4>  &solution)
{

    Vector3d p1a(v1p1[0],v1p1[1],v1p1[2]);
    Vector3d p1b(v1p2[0],v1p2[1],v1p2[2]);
    Vector3d p1c(v1p3[0],v1p3[1],v1p3[2]);

    Vector3d p0a(v2p1[0],v2p1[1],v2p1[2]);
    Vector3d p0b(v2p2[0],v2p2[1],v2p2[2]);
    Vector3d p0c(v2p3[0],v2p3[1],v2p3[2]);


    Vector3d c0 = (p0a+p0b+p0c)*(1.0/3.0);
    Vector3d c1 = (p1a+p1b+p1c)*(1.0/3.0);

    // subtract out
    p0a -= c0;
    p0b -= c0;
    p0c -= c0;
    p1a -= c1;
    p1b -= c1;
    p1c -= c1;

    Matrix3d H = p1a*p0a.transpose() + p1b*p0b.transpose() +
                 p1c*p0c.transpose();

    // do the SVD thang
    Eigen::JacobiSVD<Matrix3d> svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);
    Matrix3d V = svd.matrixV();
    Matrix3d R = V * svd.matrixU().transpose();
    double det = R.determinant();
    //ntot++;
    if (det < 0.0)
      {
        //nneg++;
        V.col(2) = V.col(2)*-1.0;
        R = V * svd.matrixU().transpose();
      }
    Vector3d tr = c0-R*c1;    // translation



    // transformation matrix, 3x4
    Eigen::Matrix<double,3,4> tfm;
    math::Matrix<double, 3, 4> TR;
    Eigen::Map<Eigen::Matrix<double,3,4,Eigen::RowMajor> > M(*TR);
    //        tfm.block<3,3>(0,0) = R.transpose();
    //        tfm.col(3) = -R.transpose()*tr;
    M.block<3,3>(0,0) = R;
    M.col(3) = tr;
   // std::cout << "Eigen \n" << tr <<std::endl<<std::endl;
   // M=tfm;
   // std::cout << TR << std::endl;
    solution=TR;

}

STEREO_NAMESPACE_END
