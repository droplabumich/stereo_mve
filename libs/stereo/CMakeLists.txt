add_library(mve_slam_stereo ransac_stereo_pose_rigid.cc	stereo_features.cc
stereo_common.cc orbextractor.cc row_matcher.cc plotting_utils.cc image_features.cc featureset_extended.cc pose_stereo_rigid.cc single_view_stereo.cc densestereo.cc stereo_matching.cc stereo_incremental.cc stereo_init_pair.cc stereo_filename_utils.cc calib.cpp)

