#include "stereo/featureset_extended.h"
SFM_NAMESPACE_BEGIN


void
FeatureSetExtended::compute_features (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib)
{
    this->colors.clear();
    this->positions.clear();
    this->width = image->width();
    this->height = image->height();
    mve::ByteImage::Ptr  out_master = mve::ByteImage::create(image->width(), image->height(), image->channels());
    if(calib.image_size.height != image->height() || calib.image_size.width != image->width() ){
        std::cerr << "Calib has incorrect image size for this image\n" << std::endl;
        exit(-1);
    }

    cv::Mat wrapped_master(image->height(), image->width(),
                    image->channels() > 1 ? CV_8UC3:CV_8U,image->begin());

    cv::Mat wrapped_master_rect(out_master->height(), out_master->width(),
                    image->channels() > 1 ? CV_8UC3:CV_8U,out_master->begin());

    cv::Mat junk(image->height(), image->width(),
                    image->channels() > 1 ? CV_8UC3:CV_8U);


    if(is_master)
        calib.rect_images(wrapped_master,wrapped_master_rect,junk,junk);
    else
        calib.rect_images(junk,junk,wrapped_master,wrapped_master_rect);
    //static_cast<FeatureSet*>(this)->opts.sift_opts.edge_ratio_threshold = 4.0f;
    static_cast<FeatureSet*>(this)->compute_features(out_master);
    //compute_orb(image,is_master,calib);

}

void
FeatureSetExtended::compute_orb (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib)
{

    cv::Mat wrapped_image(image->height(), image->width(),
                    image->channels() > 1 ? CV_8UC3:CV_8U,image->begin());

    cv::Mat wrapped_image_grey(image->height(), image->width(),
                    CV_8U);

    if(image->channels() >1 ){

        cv::cvtColor(wrapped_image, wrapped_image_grey, cv::COLOR_BGR2GRAY);
        wrapped_image=wrapped_image_grey;
    }
    orb_desc=cv::Mat();
    orb_kp.clear();
   // orb_extractor(wrapped_image,cv::Mat(),orb_kp,orb_desc);
    if(1){
        orb_desc=cv::Mat();
    orb_kp.clear();
    orb_extractor(wrapped_image,cv::Mat(),orb_kp,orb_desc);
        }else{
    detector_gen->detect(wrapped_image,orb_kp);
    extractor_gen->compute(wrapped_image,orb_kp,orb_desc);
    }

  //  /* Sort features by scale for low-res matching. */
    //std::sort(descr.begin(), descr.end(), compare_scale<sfm::Surf::Descriptor>);

    /* Prepare and copy to data structures. */
    std::size_t offset = this->positions.size();
    std::cout << "Extract SIFT+SURF " << offset << std::endl;

    this->positions.resize(offset + orb_kp.size());
    this->colors.resize(offset + orb_kp.size());
    this->num_orb_descriptors = orb_kp.size();

    std::cout << "Extract ORB " << orb_kp.size()<< std::endl;

    for (std::size_t i = 0; i < orb_kp.size(); ++i)
    {
        cv::KeyPoint const & d=orb_kp[i];
        this->positions[offset + i] = math::Vec2f(d.pt.x, d.pt.y);
        image->linear_at(d.pt.x, d.pt.y, this->colors[offset + i].begin());
    }


}
int
_ratioTest(std::vector<std::vector<cv::DMatch> >& matches, float ratio=0.6)
{

    int removed=0;

    // for all matches
    for (std::vector<std::vector<cv::DMatch> >::iterator matchIterator= matches.begin();
         matchIterator!= matches.end(); ++matchIterator) {

        /* if 2 NN have been identified */
        if (matchIterator->size() > 1) {
            /* check distance ratio */
            if ((*matchIterator)[0].distance/(*matchIterator)[1].distance > ratio) {
                matchIterator->clear(); /* remove match */
                removed++;
            }
        } else { /* does not have 2 neighbors */
            matchIterator->clear(); /* remove match */
            removed++;
        }

    }

    return removed;
}

void
_symmetryTest(const std::vector<std::vector<cv::DMatch> >& matches1,
              const std::vector<std::vector<cv::DMatch> >& matches2,
              std::vector<cv::DMatch>& symMatches)
{
    // for all matches image 1 -> image 2
    for (std::vector<std::vector<cv::DMatch> >::const_iterator matchIterator1= matches1.begin();
         matchIterator1!= matches1.end(); ++matchIterator1) {

        /* if (matchIterator1->size() < 2) // ignore deleted matches */
        /*     continue; */

        // for all matches image 2 -> image 1
        for (std::vector<std::vector<cv::DMatch> >::const_iterator matchIterator2= matches2.begin();
             matchIterator2!= matches2.end(); ++matchIterator2) {

            /* if (matchIterator2->size() < 2) // ignore deleted matches */
            /*     continue; */

            // Match symmetry test
            if ((*matchIterator1)[0].queryIdx == (*matchIterator2)[0].trainIdx  &&
                (*matchIterator2)[0].queryIdx == (*matchIterator1)[0].trainIdx) {

                // add symmetrical match
                symMatches.push_back(cv::DMatch((*matchIterator1)[0].queryIdx,
                                            (*matchIterator1)[0].trainIdx,
                                            (*matchIterator1)[0].distance));
                break; // next match in image 1 -> image 2
            }
        }
    }
}

void
FeatureSetExtended::match (FeatureSetExtended const& other, Matching::Result* result,const Stereo_Calib_CV &calib,bool between_pairs,const std::vector<int>  *master_slave_1,const std::vector<int>  *master_slave_2) const
{
    FeatureSet other_fs=other;

    Matching::Result  tmp_result;
    static_cast<const FeatureSet*>(this)->match(other_fs,&tmp_result);
    /* ORB matching. */
    sfm::Matching::Result orb_result;
    std::vector<cv::DMatch>  matches;

    if (this->num_orb_descriptors > 0)
    {
        orb_result.matches_1_2.resize(this->num_orb_descriptors,-1);
        orb_result.matches_2_1.resize(other.num_orb_descriptors,-1);

        cv::BFMatcher matcher(cv::NORM_HAMMING);
        std::vector<std::vector<cv::DMatch> > matches12, matches21;

        // match
        matcher.knnMatch(orb_desc, other.orb_desc, matches12,2);
        matcher.knnMatch(other.orb_desc, orb_desc, matches21,2);
        _ratioTest (matches12);
        _ratioTest (matches21);
        _symmetryTest (matches12, matches21, matches);

        for(std::size_t i=0; i < matches.size(); i++){
            if(matches[i].queryIdx >= 0 && matches[i].queryIdx < (int)orb_result.matches_1_2.size() &&
                    matches[i].trainIdx >= 0 && matches[i].trainIdx < (int)orb_result.matches_2_1.size()){
                orb_result.matches_1_2[matches[i].queryIdx]=matches[i].trainIdx;
                orb_result.matches_2_1[matches[i].trainIdx]=matches[i].queryIdx;
            }else{
                std::cout << "Invalid index "<< matches[i].queryIdx <<  " "<<  orb_result.matches_1_2.size() << " "<<matches[i].trainIdx << " " << orb_result.matches_2_1.size() << std::endl;
            }

        }
    }
std::cout << "Constistent ORB Matches " << Matching::count_consistent_matches(orb_result)<<"\n";

    /* Fix offsets in the matching result. */
    int offset = tmp_result.matches_2_1.size();
    std::cout << "BIG : "<<offset << " "<< this->positions.size() - this->orb_kp.size() << "\n";
    if (offset > 0)
        for (std::size_t i = 0; i < orb_result.matches_1_2.size(); ++i)
            if (orb_result.matches_1_2[i] >= 0)
                orb_result.matches_1_2[i] += offset;

    offset = tmp_result.matches_1_2.size();

    for (std::size_t i = 0; i < orb_result.matches_2_1.size(); ++i)
        if (orb_result.matches_2_1[i] >= 0)
            orb_result.matches_2_1[i] += offset;


    //std::cout << "Match SIFT+SURF: "<<valid_sift_surf_matches <<std::endl;
    std::cout << "Match ORB: "<< matches.size() << " "<<orb_result.matches_1_2.size()<<std::endl;

    /* Create a combined matching result. */
    result->matches_1_2.clear();
    result->matches_1_2.insert(result->matches_1_2.end(),
                               tmp_result.matches_1_2.begin(), tmp_result.matches_1_2.end());

    result->matches_2_1.clear();
    result->matches_2_1.insert(result->matches_2_1.end(),
                               tmp_result.matches_2_1.begin(), tmp_result.matches_2_1.end());


    std::cout << "Pre " << Matching::count_consistent_matches(*result) << std::endl;

    result->matches_1_2.insert(result->matches_1_2.end(),
                               orb_result.matches_1_2.begin(), orb_result.matches_1_2.end());
    result->matches_2_1.insert(result->matches_2_1.end(),
                               orb_result.matches_2_1.begin(), orb_result.matches_2_1.end());

    std::cout << "Post " << Matching::count_consistent_matches(*result) << std::endl;

}


SFM_NAMESPACE_END
