#ifndef FEATURESET_EXTENDED_H
#define FEATURESET_EXTENDED_H
#include "sfm/feature_set.h"
#include "stereo/orbextractor.h"
#include "opencv2/nonfree/nonfree.hpp"
#include "stereo/calib.h"
SFM_NAMESPACE_BEGIN
class FeatureSetExtended : public sfm::FeatureSet
{
public:
                                      enum FeatureTypes
                                      {
                                          FEATURE_SIFT = 1 << 0,
                                          FEATURE_SURF = 1 << 1,
                                          FEATURE_ORB  = 1 << 2,
                                          FEATURE_ALL_EXTENDED = 0xFF
                                      };

    FeatureSetExtended() ;

    struct Options : public sfm::FeatureSet::Options{
        Matching::Options orb_matching_opts;
         int nFeatures;
            float fScaleFactor ;
            int nLevels;
            int fastTh ;
            int Score;
            float max_epipolar_distance;
         FeatureTypes feature_types;
        Options() : nFeatures(1000),fScaleFactor(1.2),nLevels(8),fastTh(5),Score(0),max_epipolar_distance(3.0){}


    };
    int upto_orb_matches_2_1;
    int upto_orb_matches_1_2;
    explicit FeatureSetExtended (Options const& options);

    void match (FeatureSetExtended const& other, Matching::Result* result,const Stereo_Calib_CV &calib,bool between_pairs,const std::vector<int>  *master_slave_1=nullptr,const std::vector<int>  *master_slave_2=nullptr) const;
    void set_options (Options const& options);

    void compute_features (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib);
    void compute_orb(mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib);
    void compute_surf (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib);

    void compute_sift (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib);

    std::vector<cv::KeyPoint> orb_kp;
    std::vector<cv::KeyPoint> sift_kp;
    std::vector<cv::KeyPoint> surf_kp;
    cv::Mat orb_desc;
    cv::BFMatcher orb_bf_matcher;

    void
    match_within_pair (const std::vector<cv::KeyPoint> &keypoints_master,
                       const std::vector<cv::KeyPoint> &keypoints_slave,
                       const cv::Mat &descriptors_master,
                       const cv::Mat &descriptors_slave,
                                   const Stereo_Calib_CV &calib,

                                   double epi_threshold,
                       cv::DescriptorMatcher &matcher,
                           sfm::Matching::Result &result) const;

    void
      match_between_pairs (const std::vector<cv::KeyPoint> &keypoints_master_1,
                       const std::vector<cv::KeyPoint> &keypoints_master_2,
                       const cv::Mat &descriptors_master_1_all,
                       const cv::Mat &descriptors_master_2_all,
                          const  std::vector<int>  &master_slave_1,
                           const std::vector<int> &master_slave_2,
                                               cv::DescriptorMatcher &matcher,
                                               sfm::Matching::Result &result) const;
    private:
    cv::Mat sift_desc;
    cv::Mat surf_desc;
    std::vector<int32_t> master_inds;
    std::vector<int32_t> slave_inds;
    Options opts;
    ORBextractor orb_extractor;
    cv::SurfFeatureDetector surf_detector;
    cv::SurfDescriptorExtractor surf_extractor;
    cv::SiftFeatureDetector sift_detector;
    cv::SiftDescriptorExtractor sift_extractor;
    std::size_t num_orb_descriptors,num_sift_descriptors_extended,num_surf_descriptors_extended;
    cv::BFMatcher sift_bf_matcher;
    cv::FeatureDetector *detector_gen;
        // pointer to the feature descriptor extractor object
    cv::DescriptorExtractor *extractor_gen;


};

    inline
    FeatureSetExtended::FeatureSetExtended (void)
        : orb_extractor(opts.nFeatures,opts.fScaleFactor,opts.nLevels,opts.Score,opts.fastTh),
          num_orb_descriptors(0),sift_bf_matcher(cv::NORM_L2),orb_bf_matcher(cv::NORM_HAMMING)
    {
        int maxFeatures = opts.nFeatures;
        int rows = 5;
        int columns = 5;
        detector_gen = new cv::GridAdaptedFeatureDetector(new cv::OrbFeatureDetector(maxFeatures, 1.2f,  8,  31,
                                                                                   0, 2, cv::ORB::HARRIS_SCORE,31), maxFeatures, rows, columns);

extractor_gen = new cv::OrbDescriptorExtractor();
    }

    inline
    FeatureSetExtended::FeatureSetExtended (FeatureSetExtended::Options const& options)
        : opts(options),
          orb_extractor(opts.nFeatures,opts.fScaleFactor,opts.nLevels,opts.Score,opts.fastTh),
          num_orb_descriptors(0),sift_bf_matcher(cv::NORM_L2),orb_bf_matcher(cv::NORM_HAMMING)

    {
        int maxFeatures = opts.nFeatures;
        int rows = 5;
        int columns = 5;
        detector_gen = new cv::GridAdaptedFeatureDetector(new cv::FastFeatureDetector(1,true), 8000, rows, columns);

extractor_gen = new cv::OrbDescriptorExtractor();
    }

    inline void
    FeatureSetExtended::set_options (Options const& options)
    {
        this->opts = options;
        this->orb_extractor=ORBextractor(opts.nFeatures,opts.fScaleFactor,opts.nLevels,opts.Score,opts.fastTh);
    }
SFM_NAMESPACE_END
#endif // FEATURESET_EXTENDED_H
