#include "calib.h"
void Stereo_Calib_Result::print( std::ostream &out )
{
   out << "Left camera intrinsic parameters:\n"
       << "   f_x,f_y = "  << left_K[0] << ", "
                           << left_K[4] << "\n"
       << "   c_x,c_y = "  << left_K[2] << ", "
                           << left_K[5] << "\n"
       << "   k1,k2,k3 = " << left_d[0] << ", "
                           << left_d[1] << ", "
                           << left_d[4] << "\n"
       << "   p1,p2 = "    << left_d[2] << ", "
                           << left_d[3] << "\n"
       << "\n";

   out << "Right camera intrinsic parameters:\n"
       << "   f_x,f_y = "  << right_K[0] << ", "
                           << right_K[4] << "\n"
       << "   c_x,c_y = "  << right_K[2] << ", "
                           << right_K[5] << "\n"
       << "   k1,k2,k3 = " << right_d[0] << ", "
                           << right_d[1] << ", "
                           << right_d[4] << "\n"
       << "   p1,p2 = "    << right_d[2] << ", "
                           << right_d[3] << "\n"
       << "\n";

   out << "Extrinsic parameters:\n"
       << "   R = " << R[0] << " " << R[1] << " " << R[2] << "\n"
       << "       " << R[3] << " " << R[4] << " " << R[5] << "\n"
       << "       " << R[6] << " " << R[7] << " " << R[8] << "\n"
       << "\n"
       << "   T = " << T[0] << " "
                    << T[1] << " "
                    << T[2] << "\n";
   out << std::endl;
}
cv::Point2d PinholeCameraModel::rectifyPoint(const cv::Point2d& uv_raw) const
{


  /// @todo cv::undistortPoints requires the point data to be float, should allow double
  cv::Point2f raw32 = uv_raw, rect32;
  const cv::Mat src_pt(1, 1, CV_32FC2, &raw32.x);
  cv::Mat dst_pt(1, 1, CV_32FC2, &rect32.x);
  cv::undistortPoints(src_pt, dst_pt, K_, D_, R_, P_);
  return rect32;
}

cv::Point2d PinholeCameraModel::unrectifyPoint(const cv::Point2d& uv_rect,cv::Mat T) const
{


  // Convert to a ray
  cv::Point3d ray = projectPixelTo3dRay(uv_rect);

  // Project the ray on the image
  cv::Mat r_vec, t_vec = cv::Mat_<double>::zeros(3, 1);
  if(T.data)
    t_vec=T;
  cv::Rodrigues(R_.t(), r_vec);
  std::vector<cv::Point2d> image_point;
  cv::projectPoints(std::vector<cv::Point3d>(1, ray), r_vec, t_vec, K_, D_, image_point);

  return image_point[0];
}

cv::Point2d PinholeCameraModel::project3dToPixel(const cv::Point3d& xyz) const
{

  assert(P_(2, 3) == 0.0); // Calibrated stereo cameras should be in the same plane

  // [U V W]^T = P * [X Y Z 1]^T
  // u = U/W
  // v = V/W
  cv::Point2d uv_rect;
  uv_rect.x = (fx()*xyz.x + Tx()) / xyz.z + cx();
  uv_rect.y = (fy()*xyz.y + Ty()) / xyz.z + cy();
  return uv_rect;
}

cv::Point3d PinholeCameraModel::projectPixelTo3dRay(const cv::Point2d& uv_rect) const
{

  cv::Point3d ray;
  ray.x = (uv_rect.x - cx() - Tx()) / fx();
  ray.y = (uv_rect.y - cy() - Ty()) / fy();
  ray.z = 1.0;
  return ray;
}


void load_camera_calibration( CvSize &image_size,
                                    std::vector<double> &K,
                                    std::vector<double> &d,
                                    std::istream &in_file )
{
  // Image size
 in_file >> image_size.width >> image_size.height;

  // Intrinsic matrix
  in_file
           >> K[0] >> K[1] >>  K[2]
           >> K[3] >> K[4] >>  K[5]
           >> K[6] >> K[7] >>  K[8];

  // Distortion parameters
  in_file >>
    d[0] >> d[1] >> d[2]
     >> d[3] >> d[4];

}


