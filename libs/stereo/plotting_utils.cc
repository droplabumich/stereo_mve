#include "plotting_utils.h"
using namespace std;
using namespace cv;
namespace plotting_utils{
void convert_to_dmatch(sfm::Matching::Result &matching_result,std::vector<cv::DMatch> matches){
    matches.resize(matching_result.matches_1_2.size());
    for(int i=0; i < matching_result.matches_1_2.size(); i++){
        matches[i].trainIdx=i;
        matches[i].queryIdx=matching_result.matches_1_2[i];
    }
}

void drawKeyPoints(cv::Mat &image,
    const std::vector<cv::KeyPoint> &keypoints,
    bool colorOctave=false, bool useCartesianAngle=false)
{
  CvScalar colors[4] = {
    cvScalar(0, 0, 255),
    cvScalar(0, 255, 0),
    cvScalar(255, 0, 0),
    cvScalar(255, 255, 255)
  };

  const double PI = 3.14159265;

  vector<cv::KeyPoint>::const_iterator it;
  for(it = keypoints.begin(); it != keypoints.end(); ++it)
  {
    //float s = ((9.0f/1.2f) * it->size/10.0f) / 3.0f;
    float s = it->size / 2.f;
    //if(s < 3.f) s = 3.f;

    const CvScalar *color;
    if(!colorOctave || it->octave < 1 || it->octave > 3)
      color = &colors[3];
    else
      color = &colors[it->octave-1];

    int r1 = (int)(it->pt.y + 0.5);
    int c1 = (int)(it->pt.x + 0.5);

    cv::circle(image, cvPoint(c1, r1), (int)s, *color, 1);

    if(it->angle >= 0)
    {
      // angle is in [0..360]
      float o = (float)(it->angle * PI / 180.);
      int c2 = (int)(s * cos(o) + it->pt.x + 0.5);
      int r2;

      if(useCartesianAngle)
        r2 = (int)(- s * sin(o) + it->pt.y + 0.5);
      else
        r2 = (int)(s * sin(o) + it->pt.y + 0.5);

      cv::line(image, cvPoint(c1, r1), cvPoint(c2, r2), *color);
    }
  }
}



// ---------------------------------------------------------------------------

void drawCorrespondences(cv::Mat &image, const cv::Mat &img1,
    const cv::Mat &img2, const std::vector<cv::KeyPoint> &kp1,
    const std::vector<cv::KeyPoint> &kp2,
    const std::vector<int> &c1, const std::vector<int> &c2)
{
  int rows = img1.rows ;
  int cols = (img1.cols > img2.cols ? 2*img1.cols : 2*img2.cols);

  cv::Mat aux1, aux2;
  if(img1.channels() > 1)
    cv::cvtColor(img1, aux1, CV_RGB2GRAY);
  else
    aux1 = img1.clone();

  if(img2.channels() > 1)
    cv::cvtColor(img2, aux2, CV_RGB2GRAY);
  else
    aux2 = img2.clone();

  drawKeyPoints(aux1, kp1);
  drawKeyPoints(aux2, kp2);

  cv::Mat im = cv::Mat::zeros(rows, cols, CV_8UC1);
  IplImage ipl_im = IplImage(im);
  IplImage* ipl_ret = &ipl_im;

  CvRect roi;
  roi.x = 0;
  roi.y = 0;
  roi.width = img1.cols;
  roi.height = img1.rows;

  cvSetImageROI(ipl_ret, roi);
  IplImage ipl_aux1 = IplImage(aux1);
  cvCopyImage(&ipl_aux1, ipl_ret);

  roi.x = img1.cols;
  roi.y = 0;
  roi.width = img2.cols;
  roi.height = img2.rows;

  cvSetImageROI(ipl_ret, roi);
  IplImage ipl_aux2 = IplImage(aux2);
  cvCopyImage(&ipl_aux2, ipl_ret);

    cvResetImageROI(ipl_ret);

    // draw correspondences
    cv::cvtColor(im, image, CV_GRAY2RGB);

    for(unsigned int i = 0; i < c1.size(); ++i)
    {
        if(c1[i] < 0 || c2[i] < 0)
            continue;
      int mx = (int)kp1[ c1[i] ].pt.x;
      int my = (int)kp1[ c1[i] ].pt.y;
      int px = (int)kp2[ c2[i] ].pt.x;
      int py = (int)kp2[ c2[i] ].pt.y;

      px += img1.cols;

    CvScalar color = cvScalar(
      int(((double)rand()/((double)RAND_MAX + 1.0)) * 256.0),
      int(((double)rand()/((double)RAND_MAX + 1.0)) * 256.0),
      int(((double)rand()/((double)RAND_MAX + 1.0)) * 256.0));

    cv::line(image, cvPoint(mx, my), cvPoint(px, py), color, 1);
    }
}


std::vector<cv::KeyPoint> reDistortAndUnRectifyKeyPoints(const std::vector<cv::KeyPoint> &kp,const Stereo_Calib_CV &calib_file,bool left){
     std::vector<cv::KeyPoint> unrect_dist_kp=kp;

     for(unsigned int i=0; i<unrect_dist_kp.size(); i++)
     {

         if(left){
         unrect_dist_kp[i].pt=calib_file.leftCamRect->unrectifyPoint(   unrect_dist_kp[i].pt);
         }else{
             unrect_dist_kp[i].pt=calib_file.rightCamRect->unrectifyPoint(   unrect_dist_kp[i].pt,calib_file.T_mat);

         }
     }
    return unrect_dist_kp;
}

void drawMatches(cv::Mat &outimg,
                 const sfm::bundler::stereo::StereoViewport &view1,
                 const sfm::Matching::Result &matching_result,
                 bool redistort){

    cv::Mat im1_master,im1_slave,im2_master,im2_slave;

    cv::Mat im1_m,im1_s;
    im1_m=cv::Mat(view1.image->height(), view1.image->width(),
                  view1.image->channels() > 1 ? CV_8UC3:CV_8U,view1.image->begin());;
    im1_s=cv::Mat(view1.image_slave->height(), view1.image_slave->width(),
                  view1.image_slave->channels() > 1 ? CV_8UC3:CV_8U,view1.image_slave->begin());;;

    //view1.stereo_calibration.rect_images(im1_master, im1_m,im1_slave, im1_s);
    //view1.stereo_calibration.rect_images(im2_master, im2_m,im2_slave, im2_s);

    Mat im1_match_img;
    std::vector<cv::KeyPoint> redist_kp_master1;
    std::vector<cv::KeyPoint> redist_kp_slave1;
    if(redistort){
        redist_kp_master1=reDistortAndUnRectifyKeyPoints(view1.features.orb_kp,view1.stereo_calibration,true);
        redist_kp_slave1=reDistortAndUnRectifyKeyPoints(view1.features_slave.orb_kp,view1.stereo_calibration,false);
    }else{
        redist_kp_master1=view1.features.orb_kp;
        redist_kp_slave1=view1.features_slave.orb_kp;
    }

    std::vector<int> c1;
    std::vector<int> c2;
    for(int i=0; i< matching_result.matches_1_2.size(); i++){
        if(matching_result.matches_1_2[i]<0)
            continue;
        c1.push_back(i);
        c2.push_back(matching_result.matches_1_2[i]);

    }
    drawCorrespondences(outimg,im1_m,im1_s,redist_kp_master1,redist_kp_slave1,c1,c2);

}
void drawPair2PairMatches(cv::Mat &outimg, mve::ByteImage::Ptr im1_master_bi,
                          mve::ByteImage::Ptr im1_slave_bi,
                          mve::ByteImage::Ptr im2_master_bi,
                          mve::ByteImage::Ptr im2_slave_bi,
                          const sfm::bundler::stereo::StereoViewport &view1,const sfm::bundler::stereo::StereoViewport &view2,
                          const sfm::Matching::Result &matching_result,
                          const Stereo_Calib_CV &calib_file,bool redistort){

    cv::Mat im1_master,im1_slave,im2_master,im2_slave;

    cv::Mat im1_m,im1_s,im2_m,im2_s;
    if(im1_master_bi == nullptr){
        im1_m=cv::Mat(calib_file.image_size,CV_8UC3);
         im1_m=cv::Scalar(20,20,20);
        im2_m= im1_m;
        im1_s= im1_m;
        im2_s= im1_m;
    }else{
        if(redistort){
            im1_m=cv::Mat(im1_master_bi->height(), im1_master_bi->width(),
                   im1_master_bi->channels() > 1 ? CV_8UC3:CV_8U,im1_master_bi->begin());;
            im1_s=cv::Mat(im1_slave_bi->height(), im1_slave_bi->width(),
                          im1_slave_bi->channels() > 1 ? CV_8UC3:CV_8U,im1_slave_bi->begin());;;
            im2_m=cv::Mat(im2_master_bi->height(), im2_master_bi->width(),
                          im2_master_bi->channels() > 1 ? CV_8UC3:CV_8U,im2_master_bi->begin());;;
            im2_s=cv::Mat(im2_slave_bi->height(), im2_slave_bi->width(),
                          im2_slave_bi->channels() > 1 ? CV_8UC3:CV_8U,im2_slave_bi->begin());;;
        }else{

            calib_file.rect_images(im1_master, im1_m,im1_slave, im1_s);
            calib_file.rect_images(im2_master, im2_m,im2_slave, im2_s);



        }


    }

    Mat im1_match_img;
    std::vector<cv::KeyPoint> redist_kp_master1;
    std::vector<cv::KeyPoint> redist_kp_slave1;
    for(int i = 0; i <view1.features.positions.size(); i++){
        cv::KeyPoint kp_master;
        kp_master.pt=cv::Point2f(view1.features.positions[i][0],view1.features.positions[i][1]);
        redist_kp_master1.push_back(kp_master);
    }
    for(int i = 0; i < view1.features_slave.positions.size(); i++){
        cv::KeyPoint kp_slave;
        kp_slave.pt=cv::Point2f(view1.features_slave.positions[i][0],view1.features_slave.positions[i][1]);
        redist_kp_slave1.push_back(kp_slave);

    }
    if(redistort){
        redist_kp_master1=reDistortAndUnRectifyKeyPoints(redist_kp_master1,calib_file,true);
        redist_kp_slave1=reDistortAndUnRectifyKeyPoints(redist_kp_slave1,calib_file,false);
    }

    std::vector<int> c1;
    std::vector<int> c2;
    for(int i=0; i< view1.matches_master_slave.size(); i++){

        c1.push_back(i);
        c2.push_back(view1.matches_master_slave[i]);
    }
    drawCorrespondences(im1_match_img,im1_m,im1_s,redist_kp_master1,redist_kp_slave1,c1,c2);
    std::vector<cv::KeyPoint> redist_kp_master2;
    std::vector<cv::KeyPoint> redist_kp_slave2;
    Mat im2_match_img;

    c1.clear();
    c2.clear();

    for(int i = 0; i < view2.features.positions.size(); i++){
        cv::KeyPoint kp_master;
        kp_master.pt=cv::Point2f(view2.features.positions[i][0],view2.features.positions[i][1]);
        redist_kp_master2.push_back(kp_master);
}
    for(int i = 0; i < view2.features_slave.positions.size(); i++){

        cv::KeyPoint kp_slave;
        kp_slave.pt=cv::Point2f(view2.features_slave.positions[i][0],view2.features_slave.positions[i][1]);
        redist_kp_slave2.push_back(kp_slave);

    }
    if(redistort){
        redist_kp_master2=reDistortAndUnRectifyKeyPoints(redist_kp_master2,calib_file,true);
        redist_kp_slave2=reDistortAndUnRectifyKeyPoints(redist_kp_slave2,calib_file,false);
    }


    for(int i=0; i< view2.matches_master_slave.size(); i++){

        c1.push_back(i);
        c2.push_back(view2.matches_master_slave[i]);
    }
    drawCorrespondences(im2_match_img,im2_m,im2_s,redist_kp_master2,redist_kp_slave2,c1,c2);


    Size sz1 = im1_match_img.size();
    Size sz2 = im2_match_img.size();

    Mat im3(sz1.height+sz2.height, sz1.width, CV_8UC3);
    Mat top(im3, Rect(0, 0, sz1.width, sz1.height));
    im1_match_img.copyTo(top);

    Mat bottom(im3, Rect(0, sz1.height, sz2.width, sz2.height));
    im2_match_img.copyTo(bottom);
    for(unsigned int i = 0; i < matching_result.matches_1_2.size(); ++i)
    {
        if( matching_result.matches_1_2[i] < 0)
            continue;
      int mx = (int)redist_kp_master1[ i ].pt.x;
      int my = (int)redist_kp_master1[ i ].pt.y;
      int px = (int)redist_kp_master2[ matching_result.matches_1_2[i] ].pt.x;
      int py = (int)redist_kp_master2[  matching_result.matches_1_2[i]].pt.y;

      py += im1_match_img.rows;

    CvScalar color = cvScalar(
      int(((double)rand()/((double)RAND_MAX + 1.0)) * 256.0),
      int(((double)rand()/((double)RAND_MAX + 1.0)) * 256.0),
      int(((double)rand()/((double)RAND_MAX + 1.0)) * 256.0));
    /*cv::LineIterator it(im3, cvPoint(mx, my), cvPoint(px, py), 8);
    for(int i = 0; i < it.count; i++,it++)
        if ( i%5!=0 ) {(*it)[0] = 200;}*/
     cv::line(im3, cvPoint(mx, my), cvPoint(px, py), cvScalar(255.0,255.0,255.0), 3);
    }
    outimg=im3;
}

};
