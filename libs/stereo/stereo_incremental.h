/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef STEREO_INCREMENTAL_HEADER
#define STEREO_INCREMENTAL_HEADER

#include "mve/bundle.h"
#include "sfm/fundamental.h"
#include "sfm/ransac_fundamental.h"
#include "sfm/ransac_pose_p3p.h"
#include "sfm/bundler_common.h"
#include "sfm/camera_pose.h"
#include "sfm/defines.h"
#include "sfm/bundler_incremental.h"
#include "stereo/defines.h"
#include "stereo/stereo_features.h"
SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
/**
 * Bundler Component: Incremental stereo structure-from-motion.
 */
class Incremental : public sfm::bundler::Incremental
{
public:
    struct Options : public sfm::bundler::Incremental::Options
    {
        Options (void);


    };

public:
    explicit Incremental (Options const& options);

    /**
     * Initializes the incremental bundler and sets required data.
     * - viewports: per-viewport colors, positions, track IDs, and initial
     *   focal length is required. A good initial camera pair is required
     *   so that initial tracks can be triangulated. Radial distortion and
     *   the camera pose is regularly updated.
     * - tracks: The tracks are triangulated and regularly updated.
     */
    void initialize (StereoViewportList* viewports, TrackList* tracks, StereoPairwiseMatching* pairwise_matching);
    /** Returns whether the incremental SfM has been initialized. */
    bool is_initialized (void) const;

    /** Returns a list of suitable view ID or emtpy list on failure. */
    void find_next_views (std::vector<int>* next_views);
    /** Incrementally adds the given view to the bundle. */
    bool reconstruct_next_view (int view_id);
    /** Triangulates tracks without 3D position and at least N views. */
    void triangulate_new_tracks (void);
    /** Deletes tracks with a large reprojection error. */
    void invalidate_large_error_tracks (double threshold);
    /** Runs bundle adjustment on both, structure and motion. */
    void bundle_adjustment_full (void);
    /** Runs bundle adjustment on a single camera without structure. */
    void bundle_adjustment_single_cam (int view_id);

    /** Transforms the bundle for numerical stability. */
    void normalize_scene (void);
    /** Computes a bundle from all viewports and reconstructed tracks. */
    mve::Bundle::Ptr create_bundle (void) const;
    void set_last_view_id(int view_id){last_view_id=view_id;}
    void get_total_error (void);
    void add_fixed_stereo_views(void);
private:
    void bundle_adjustment_intern (int single_camera_ba);
    void bundle_adjustment_intern_pba (int single_camera_ba);

private:
    Options opts;
    StereoViewportList* viewports;
    TrackList* tracks;
    StereoPairwiseMatching* pairwise_matching;
    int last_view_id;
};

/* ------------------------ Implementation ------------------------ */

inline
Incremental::Options::Options (void)
{
}

inline
Incremental::Incremental (Options const& options)
    : sfm::bundler::Incremental(options),opts(options)
    , viewports(NULL)
    , tracks(NULL)
{
}

inline bool
Incremental::is_initialized (void) const
{
    return this->viewports != NULL
        && this->tracks != NULL;
}
STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END

#endif /* STEREO_INCREMENTAL_HEADER */
