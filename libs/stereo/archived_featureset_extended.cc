#include "stereo/featureset_extended.h"
#include "stereo/stereo_common.h"

SFM_NAMESPACE_BEGIN


void
FeatureSetExtended::compute_features (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib)
{
    this->colors.clear();
    this->positions.clear();
    this->width = image->width();
    this->height = image->height();


    //static_cast<FeatureSet*>(this)->compute_features(image);
    compute_sift(image,is_master,calib);
    compute_surf(image,is_master,calib);
    compute_orb(image,is_master,calib);

}

void
FeatureSetExtended::compute_orb (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib)
{

    cv::Mat wrapped_image(image->height(), image->width(),
                    image->channels() > 1 ? CV_8UC3:CV_8U,image->begin());

    cv::Mat wrapped_image_grey(image->height(), image->width(),
                    CV_8U);

    if(image->channels() >1 ){

        cv::cvtColor(wrapped_image, wrapped_image_grey, cv::COLOR_BGR2GRAY);
        wrapped_image=wrapped_image_grey;


    }/*else{
        cv::Mat tmp_img;
        cv::resize(wrapped_image,tmp_img,cv::Size(),0.5,0.5,cv::INTER_CUBIC);
        cv::resize(tmp_img,wrapped_image,cv::Size(),2.0,2.0,cv::INTER_CUBIC);
    }*/
    if(1){
        orb_desc=cv::Mat();
    orb_kp.clear();
    orb_extractor(wrapped_image,cv::Mat(),orb_kp,orb_desc);
}else{
    detector_gen->detect(wrapped_image,orb_kp);
    extractor_gen->compute(wrapped_image,orb_kp,orb_desc);
    }
  //  /* Sort features by scale for low-res matching. */
    //std::sort(descr.begin(), descr.end(), compare_scale<sfm::Surf::Descriptor>);

    /* Prepare and copy to data structures. */
    std::size_t offset = this->positions.size();
    std::cout << "Extract SIFT+SURF " << offset << std::endl;

    this->positions.resize(offset + orb_kp.size());
    this->colors.resize(offset + orb_kp.size());
    this->num_orb_descriptors = orb_kp.size();

    std::cout << "Extract ORB " << orb_kp.size()<< std::endl;

    for (std::size_t i = 0; i < orb_kp.size(); ++i)
    {
        cv::KeyPoint & d=orb_kp[i];
        image->linear_at(d.pt.x, d.pt.y, this->colors[offset + i].begin());
        cv::Point2f pos_rect;
        if(is_master)
            pos_rect=calib.leftCamRect->rectifyPoint(d.pt);
        else
            pos_rect=calib.rightCamRect->rectifyPoint(d.pt);
        d.pt=pos_rect;
        this->positions[offset + i] = math::Vec2f(pos_rect.x, pos_rect.y);
    }


}

void
FeatureSetExtended::compute_sift (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib)
{

    cv::Mat wrapped_image(image->height(), image->width(),
                    image->channels() > 1 ? CV_8UC3:CV_8U,image->begin());

    cv::Mat wrapped_image_grey(image->height(), image->width(),
                    CV_8U);

    if(image->channels() >1 ){

        cv::cvtColor(wrapped_image, wrapped_image_grey, cv::COLOR_BGR2GRAY);
        wrapped_image=wrapped_image_grey;
    }
    sift_desc=cv::Mat();
    sift_kp.clear();
    sift_detector.compute(wrapped_image,sift_kp,sift_desc);

  //  /* Sort features by scale for low-res matching. */
    //std::sort(descr.begin(), descr.end(), compare_scale<sfm::sift::Descriptor>);

    /* Prepare and copy to data structures. */
    std::size_t offset = this->positions.size();
    std::cout << "Extract SIFT+SURF " << offset << std::endl;

    this->positions.resize(offset + sift_kp.size());
    this->colors.resize(offset + sift_kp.size());
    this->num_sift_descriptors_extended = sift_kp.size();

    std::cout << "Extract sift " << sift_kp.size()<< std::endl;

    for (std::size_t i = 0; i < sift_kp.size(); ++i)
    {
        cv::KeyPoint const & d=sift_kp[i];
        this->positions[offset + i] = math::Vec2f(d.pt.x, d.pt.y);
        image->linear_at(d.pt.x, d.pt.y, this->colors[offset + i].begin());
    }


}
void
FeatureSetExtended::compute_surf (mve::ByteImage::Ptr image,bool is_master,const Stereo_Calib_CV &calib)
{

    cv::Mat wrapped_image(image->height(), image->width(),
                    image->channels() > 1 ? CV_8UC3:CV_8U,image->begin());

    cv::Mat wrapped_image_grey(image->height(), image->width(),
                    CV_8U);

    if(image->channels() >1 ){

        cv::cvtColor(wrapped_image, wrapped_image_grey, cv::COLOR_BGR2GRAY);
        wrapped_image=wrapped_image_grey;
    }
    surf_desc=cv::Mat();
    surf_kp.clear();
    surf_detector.compute(wrapped_image,surf_kp,surf_desc);

  //  /* Sort features by scale for low-res matching. */
    //std::sort(descr.begin(), descr.end(), compare_scale<sfm::Surf::Descriptor>);

    /* Prepare and copy to data structures. */
    std::size_t offset = this->positions.size();
    std::cout << "Extract SIFT+SURF " << offset << std::endl;

    this->positions.resize(offset + surf_kp.size());
    this->colors.resize(offset + surf_kp.size());
    this->num_surf_descriptors_extended = surf_kp.size();

    std::cout << "Extract SURF " << surf_kp.size()<< std::endl;

    for (std::size_t i = 0; i < surf_kp.size(); ++i)
    {
        cv::KeyPoint const & d=surf_kp[i];
        this->positions[offset + i] = math::Vec2f(d.pt.x, d.pt.y);
        image->linear_at(d.pt.x, d.pt.y, this->colors[offset + i].begin());
    }


}
int
_ratioTest(std::vector<std::vector<cv::DMatch> >& matches, float ratio=0.6)
{

    int removed=0;

    // for all matches
    for (std::vector<std::vector<cv::DMatch> >::iterator matchIterator= matches.begin();
         matchIterator!= matches.end(); ++matchIterator) {

        /* if 2 NN have been identified */
        if (matchIterator->size() > 1) {
            /* check distance ratio */
            if ((*matchIterator)[0].distance/(*matchIterator)[1].distance > ratio) {
                matchIterator->clear(); /* remove match */
                removed++;
            }
        } else { /* does not have 2 neighbors */
            matchIterator->clear(); /* remove match */
            removed++;
        }

    }

    return removed;
}

void
_symmetryTest(const std::vector<std::vector<cv::DMatch> >& matches1,
              const std::vector<std::vector<cv::DMatch> >& matches2,
              std::vector<cv::DMatch>& symMatches)
{
    // for all matches image 1 -> image 2
    for (std::vector<std::vector<cv::DMatch> >::const_iterator matchIterator1= matches1.begin();
         matchIterator1!= matches1.end(); ++matchIterator1) {

        /* if (matchIterator1->size() < 2) // ignore deleted matches */
        /*     continue; */

        // for all matches image 2 -> image 1
        for (std::vector<std::vector<cv::DMatch> >::const_iterator matchIterator2= matches2.begin();
             matchIterator2!= matches2.end(); ++matchIterator2) {

            /* if (matchIterator2->size() < 2) // ignore deleted matches */
            /*     continue; */

            // Match symmetry test
            if ((*matchIterator1)[0].queryIdx == (*matchIterator2)[0].trainIdx  &&
                (*matchIterator2)[0].queryIdx == (*matchIterator1)[0].trainIdx) {

                // add symmetrical match
                symMatches.push_back(cv::DMatch((*matchIterator1)[0].queryIdx,
                                            (*matchIterator1)[0].trainIdx,
                                            (*matchIterator1)[0].distance));
                break; // next match in image 1 -> image 2
            }
        }
    }
}

std::vector<int32_t>
prune_candidate_features_epi (cv::Point2d uv_1, const std::vector<cv::KeyPoint> &feats_2,
                              const Stereo_Calib_CV &calib, double epi_threshold)
{
    std::vector<int32_t> inliers;

    for (int32_t featIndex=0; featIndex<(int)feats_2.size(); ++featIndex) {
        if (calib.is_vertical) {
            double error = fabs (feats_2[featIndex].pt.x - uv_1.x);
            if (error < epi_threshold) {
                inliers.push_back (featIndex);
            }
        } else {
            double error = fabs (feats_2[featIndex].pt.y - uv_1.y);
            if (error < epi_threshold) {
                inliers.push_back (featIndex);
            }
        }
    }

    return inliers;
}
void
FeatureSetExtended::match_within_pair (const std::vector<cv::KeyPoint> &keypoints_master,
                   const std::vector<cv::KeyPoint> &keypoints_slave,
                   const cv::Mat &descriptors_master,
                   const cv::Mat &descriptors_slave,
                               const Stereo_Calib_CV &calib,
                               double epi_threshold,
                                       cv::DescriptorMatcher &matcher,
                                       sfm::Matching::Result &result) const
{
    std::vector< std::vector<cv::DMatch> > matches;
   matcher.knnMatch( descriptors_master, descriptors_slave, matches, 500 );

    //look whether the match is inside a defined area of the image
    //only 25% of maximum of possible distance
    double tresholdDist = 0.25 * sqrt(double(height*height + width*width));

    std::vector< cv::DMatch > good_matches2;
    good_matches2.reserve(matches.size());
    for (size_t i = 0; i < matches.size(); ++i)
    {
        for (int j = 0; j < matches[i].size(); j++)
        {
            cv::Point2f from = keypoints_master[matches[i][j].queryIdx].pt;
            cv::Point2f to = keypoints_slave[matches[i][j].trainIdx].pt;

            //calculate local distance for each possible match
            double dist = sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y));

            //save as best match if local distance is in specified area and on same height
            if (dist < tresholdDist && abs(from.y-to.y)<5)
            {
                good_matches2.push_back(matches[i][j]);
                j = matches[i].size();

            }
        }
    }
    printf("JOIN: %d %d\n",        result.matches_1_2.size(),        result.matches_2_1.size());
    for(size_t i=0; i < good_matches2.size(); i++){
        if(good_matches2[i].queryIdx >= 0 && good_matches2[i].queryIdx < (int)result.matches_1_2.size() &&
                good_matches2[i].trainIdx >= 0 && good_matches2[i].trainIdx < (int)result.matches_2_1.size()){
        result.matches_1_2[good_matches2[i].queryIdx]=good_matches2[i].trainIdx;
        result.matches_2_1[good_matches2[i].trainIdx]=good_matches2[i].queryIdx;
        }
        else{
            std::cout << "failed " <<good_matches2[i].queryIdx << " "<<good_matches2[i].trainIdx<< " "<< num_orb_descriptors << std::endl;
        }
    }
#if 0
    std::size_t valid=0;
    std::vector<int32_t> inliers;
    for (int32_t i=0; i<(int)keypoints_master.size(); ++i) {
        cv::Mat inlier_descriptors_slave;
        cv::Mat curr_master_descriptor = descriptors_master.row (i);
        inliers = prune_candidate_features_epi (cv::Point2d(keypoints_master[i].pt.x,keypoints_master[i].pt.y), keypoints_slave, calib, epi_threshold);
        for (std::vector<int32_t>::const_iterator it=inliers.begin (); it!=inliers.end (); ++it)
            inlier_descriptors_slave.push_back (descriptors_slave.row (*it));
        if (inliers.size () > 0) {
            std::vector<cv::DMatch> matches_;
            matcher.match(curr_master_descriptor, inlier_descriptors_slave,matches_);

            if (matches_.size () == 1) {

               // master_inds.push_back (i);
              //  slave_inds.push_back (inliers[matches_[0].trainIdx]);
                result.matches_1_2[i]=inliers[matches_[0].trainIdx];
                result.matches_2_1[result.matches_1_2[i]]=i;
                std::cout << result.matches_1_2[i]<<" " <<result.matches_2_1[inliers[matches_[0].trainIdx]]<< " " << i<< " ";
                std::cout << (result.matches_1_2[i] != -1
                            && result.matches_2_1[result.matches_1_2[i]] ==i) << std::endl;
                valid++;
            }
        }
    }
    std::cout << "!! Number of valid: " << valid << " "<<Matching::count_consistent_matches(    result)<<std::endl;
#endif
}

void
  FeatureSetExtended::match_between_pairs (const std::vector<cv::KeyPoint> &keypoints_master_1,
                   const std::vector<cv::KeyPoint> &keypoints_master_2,
                   const cv::Mat &descriptors_master_1,
                   const cv::Mat &descriptors_master_2,
                             /*    const std::vector<int32_t> &inds_master_1,
                                 const std::vector<int32_t> &inds_slave_1,
                                 const std::vector<int32_t> &inds_master_2,
                                 const std::vector<int32_t> &inds_slave_2,*/
                                       const     std::vector<int>  &master_slave_1,
const                                          std::vector<int> &master_slave_2,
                                           cv::DescriptorMatcher &matcher,
                                           sfm::Matching::Result &result) const
{

  /*  std::vector<int32_t> inliers1(master_slave_1.size (),-1);
     std::vector<int32_t>  inliers2(master_slave_2.size (),-1);
    cv::Mat descriptors_master_1, descriptors_master_2;*/
    size_t num_valid=0;
    cv::Mat mask_for_stereo_valid = cv::Mat::zeros(keypoints_master_1.size(),keypoints_master_2.size(),CV_8U);
    for (size_t i=0; i<mask_for_stereo_valid.rows; ++i){
        if(master_slave_1[i] >=0){
            for(int j=0; j < mask_for_stereo_valid.cols; j++ ){
               if(master_slave_2[j] >=0){
                  mask_for_stereo_valid.at<uchar>(i,j) = 1;
                  num_valid++;
               }
            }
          }



    }
        /*if(1){//     if(master_slave_1[i] >=0){
            inliers1[i]=descriptors_master_1.rows;
            descriptors_master_1.push_back (descriptors_master_1_all.row (i));
        }
    }
    for (size_t i=0; i<master_slave_2.size (); ++i){
         if(1){//    if(master_slave_2[i] >=0){
            inliers2[i]=descriptors_master_2.rows;
            descriptors_master_2.push_back (descriptors_master_2_all.row (i));
        }
    }*/
    if(num_valid == 0)
      return;

    std::vector<std::vector<cv::DMatch> > matches12, matches21;
    std::vector< cv::DMatch > matches;


    // match
    matcher.knnMatch(descriptors_master_1, descriptors_master_2, matches12,2);
    matcher.knnMatch(descriptors_master_2, descriptors_master_1, matches21,2);
    _ratioTest (matches12);
    _ratioTest (matches21);
    _symmetryTest (matches12, matches21, matches);

    for(size_t i=0; i < matches.size(); i++){
        if(matches[i].queryIdx >= 0 && matches[i].queryIdx < (int)result.matches_1_2.size() &&
                matches[i].trainIdx >= 0 && matches[i].trainIdx < (int)result.matches_2_1.size()){
        result.matches_1_2[matches[i].queryIdx]=matches[i].trainIdx;
        result.matches_2_1[matches[i].trainIdx]=matches[i].queryIdx;
        }
        else{
            std::cout << "failed " <<matches[i].queryIdx << " "<<matches[i].trainIdx<< " "<< result.matches_1_2.size() << " " <<  result.matches_2_1.size()<< std::endl;
        }
    }

}
#if 0
void
FeatureSetExtended::match (FeatureSetExtended const& other, Matching::Result* result,const Stereo_Calib_CV &calib,bool between_pairs,const std::vector<int>  *master_slave_1,const std::vector<int>  *master_slave_2) const
{
    FeatureSet other_fs=other;

    Matching::Result  tmp_result;
   // static_cast<const FeatureSet*>(this)->match(other_fs,&tmp_result);

    /* ORB matching. */
    sfm::Matching::Result orb_result;
    std::vector<cv::DMatch>  matches;

    if (this->num_orb_descriptors > 0)
    {
        orb_result.matches_1_2.resize(this->num_orb_descriptors,-1);
        orb_result.matches_2_1.resize(this->num_orb_descriptors,-1);

        cv::BFMatcher matcher(cv::NORM_HAMMING);
        std::vector<std::vector<cv::DMatch> > matches12, matches21;

        // match
        matcher.knnMatch(orb_desc, other.orb_desc, matches12,2);
        matcher.knnMatch(other.orb_desc, orb_desc, matches21,2);
        _ratioTest (matches12);
        _ratioTest (matches21);
        _symmetryTest (matches12, matches21, matches);

        for(std::size_t i=0; i < matches.size(); i++){
            if(matches[i].queryIdx >= 0 && matches[i].queryIdx < (int)num_orb_descriptors &&
                    matches[i].trainIdx >= 0 && matches[i].trainIdx < (int)num_orb_descriptors){
                orb_result.matches_1_2[matches[i].queryIdx]=matches[i].trainIdx;
                orb_result.matches_2_1[matches[i].trainIdx]=matches[i].queryIdx;
            }

        }
    }
    /* Fix offsets in the matching result. */
    int offset = tmp_result.matches_2_1.size();
    if (offset > 0)
        for (std::size_t i = 0; i < orb_result.matches_1_2.size(); ++i)
            if (orb_result.matches_1_2[i] >= 0)
                orb_result.matches_1_2[i] += offset;

    offset = tmp_result.matches_1_2.size();

    for (std::size_t i = 0; i < orb_result.matches_2_1.size(); ++i)
        if (orb_result.matches_2_1[i] >= 0)
            orb_result.matches_2_1[i] += offset;


    //std::cout << "Match SIFT+SURF: "<<valid_sift_surf_matches <<std::endl;
    std::cout << "Match ORB: "<< matches.size() << " "<<orb_result.matches_1_2.size()<<std::endl;

    /* Create a combined matching result. */
    result->matches_1_2.clear();
    result->matches_1_2.insert(result->matches_1_2.end(),
                               tmp_result.matches_1_2.begin(), tmp_result.matches_1_2.end());

    result->matches_2_1.clear();
    result->matches_2_1.insert(result->matches_2_1.end(),
                               tmp_result.matches_2_1.begin(), tmp_result.matches_2_1.end());


    std::cout << "Pre " << Matching::count_consistent_matches(*result) << std::endl;

    result->matches_1_2.insert(result->matches_1_2.end(),
                               orb_result.matches_1_2.begin(), orb_result.matches_1_2.end());
    result->matches_2_1.insert(result->matches_2_1.end(),
                               orb_result.matches_2_1.begin(), orb_result.matches_2_1.end());

    std::cout << "Post " << Matching::count_consistent_matches(*result) << std::endl;
}
#else
void
FeatureSetExtended::match (FeatureSetExtended const& other, Matching::Result* result,const Stereo_Calib_CV &calib,bool between_pairs,const std::vector<int>  *master_slave_1,const std::vector<int>  *master_slave_2) const
{
    const
    FeatureSet other_fs=other;

    Matching::Result  tmp_result;
    static_cast<const FeatureSet*>(this)->match(other_fs,&tmp_result);

    /* ORB matching. */
    sfm::Matching::Result orb_result;
    std::vector<cv::DMatch>  matches;

    if (this->num_orb_descriptors > 0)
    {
        orb_result.matches_1_2.resize(this->num_orb_descriptors,-1);
        orb_result.matches_2_1.resize(other.num_orb_descriptors,-1);

        cv::BFMatcher matcher(cv::NORM_HAMMING);

       if(between_pairs){
           if(master_slave_1 == nullptr)
           {
               throw std::invalid_argument("master_slave_1 is null");
           }

           if(master_slave_2 == nullptr){
               throw std::invalid_argument("master_slave_1 is null");
           }
           match_between_pairs(orb_kp,
                               other.orb_kp,
                              orb_desc,
                              other.orb_desc,
                              *master_slave_1,
                              *master_slave_2,
                              matcher,
                                             orb_result);
       }else{
           match_within_pair (orb_kp,
                               other.orb_kp,
                              orb_desc,
                              other.orb_desc,
                                          calib,
                                            opts.max_epipolar_distance,
                                                 matcher,
                                             orb_result);
           /*const std::vector<int>  ms1(orb_kp.size(),1);
           const std::vector<int>  ms2(other.orb_kp.size(),1);
           match_between_pairs(orb_kp,
                               other.orb_kp,
                              orb_desc,
                              other.orb_desc,
                              ms1,
                              ms2,
                              matcher,
                                             orb_result);*/
       }
    }


    /* Fix offsets in the matching result. */
    int offset = tmp_result.matches_2_1.size();
    if (offset > 0)
        for (std::size_t i = 0; i < orb_result.matches_1_2.size(); ++i)
            if (orb_result.matches_1_2[i] >= 0)
                orb_result.matches_1_2[i] += offset;

    offset = tmp_result.matches_1_2.size();

    for (std::size_t i = 0; i < orb_result.matches_2_1.size(); ++i)
        if (orb_result.matches_2_1[i] >= 0)
            orb_result.matches_2_1[i] += offset;


    //std::cout << "Match SIFT+SURF: "<<valid_sift_surf_matches <<std::endl;
    std::cout << "Match ORB: "<< matches.size() << " "<<orb_result.matches_1_2.size()<<std::endl;

    /* Create a combined matching result. */
    result->matches_1_2.clear();
    result->matches_1_2.insert(result->matches_1_2.end(),
                               tmp_result.matches_1_2.begin(), tmp_result.matches_1_2.end());

    result->matches_2_1.clear();
    result->matches_2_1.insert(result->matches_2_1.end(),
                               tmp_result.matches_2_1.begin(), tmp_result.matches_2_1.end());


    std::cout << "Pre " << Matching::count_consistent_matches(*result) << std::endl;

    result->matches_1_2.insert(result->matches_1_2.end(),
                               orb_result.matches_1_2.begin(), orb_result.matches_1_2.end());
    result->matches_2_1.insert(result->matches_2_1.end(),
                               orb_result.matches_2_1.begin(), orb_result.matches_2_1.end());

    std::cout << "Post " << Matching::count_consistent_matches(*result) << std::endl;

}
#endif

SFM_NAMESPACE_END
