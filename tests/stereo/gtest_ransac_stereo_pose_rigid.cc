// Test cases for perspective 3 point algorithms.
// Written by Simon Fuhrmann.

#include <gtest/gtest.h>

#include <vector>

#include "math/vector.h"
#include "math/matrix.h"
#include "math/quaternion.h"
#include "sfm/camera_pose.h"
#include "sfm/fundamental.h"
#include "sfm/ransac.h"
#include "sfm/ransac_fundamental.h"
#include "sfm/triangulate.h"
#include "stereo/stereo_features.h"
#include "stereo/ransac_stereo_pose_rigid.h"
#include "stereo/pose_stereo_rigid.h"

using namespace std;
namespace
{

void
fill_ground_truth_pose (sfm::CameraPose* pose1_master,sfm::CameraPose* pose1_slave, sfm::CameraPose* pose2_master, sfm::CameraPose* pose2_slave,double tx)
{
    // Calibration with focal lenght 1 and 800x600 image.
    // The first camera looks straight along the z axis.
    if (pose1_master != NULL)
    {
        pose1_master->set_k_matrix(800, 800 / 2, 600 / 2);
        math::matrix_set_identity(*pose1_master->R, 3);
        pose1_master->t.fill(0.0);
        *pose1_slave=*pose1_master;
        pose1_slave->t[0]+=tx;

    }

    // The second camera is at (1,0.25,0) and rotated 45deg to the left.
    if (pose2_master != NULL)
    {
        pose2_master->set_k_matrix(800, 800 / 2, 600 / 2);
        pose2_master->R.fill(0.0);
        double const angle = MATH_PI / 4;
        pose2_master->R(0,0) = std::cos(angle); pose2_master->R(0,2) = std::sin(angle);
        pose2_master->R(1,1) = 1.0;
        pose2_master->R(2,0) = -std::sin(angle); pose2_master->R(2,2) = std::cos(angle);
        pose2_master->t.fill(0.0); pose2_master->t[2]=10.0; ;//= 1.0;pose2_master->t[1] = 0.25;
        //pose2_master->t = pose2_master->R * -pose2_master->t;
        *pose2_slave=*pose2_master;
        pose2_slave->t[0]+=tx;

    }
}
void
fill_eight_random_points (std::vector<math::Vec3d>* points)
{
    points->push_back(math::Vec3d(-0.31, -0.42, 1.41));
    points->push_back(math::Vec3d( 0.04,  0.01, 0.82));
    points->push_back(math::Vec3d(-0.25, -0.24, 1.25));
    points->push_back(math::Vec3d( 0.47,  0.22, 0.66));
    points->push_back(math::Vec3d( 0.13,  0.03, 0.89));
    points->push_back(math::Vec3d(-0.13, -0.46, 1.15));
    points->push_back(math::Vec3d( 0.21, -0.23, 1.33));
    points->push_back(math::Vec3d(-0.42,  0.38, 0.62));
}

}
TEST(RANSACPoseTest, SyntheticPoseTest1)
{
    // This test computes from a given pose the fundamental matrix,
    // then the essential matrix, then recovers the original pose.
    sfm::CameraPose pose1_master,pose1_slave, pose2_master,pose2_slave;
    double tx = 0.05; //Baseline for stereo pair

    fill_ground_truth_pose(&pose1_master,&pose1_slave, &pose2_master,&pose2_slave,tx);

    // Eight "random" 3D points.
    std::vector<math::Vec3d> points3d;

    math::Matrix<double, 3, 4> gt_pose;
    gt_pose = pose2_master.R.hstack(pose2_master.t);

    std::vector<math::Vec3d> points3d_frame2;

    fill_eight_random_points(&points3d);
    for(int i=0; i < points3d.size(); i++){

        points3d_frame2.push_back(gt_pose*math::Vec4d(points3d[i],1.0));
    }

    // Re-project in images using ground truth pose.
    math::Matrix<double, 3, 8> points2d_v1_master, points2d_v1_slave,points2d_v2_master,points2d_v2_slave;
    for (int i = 0; i < 8; ++i)
    {
        math::Vec3d p1_master = pose1_master.K * (pose1_master.R * points3d[i] + pose1_master.t);
        math::Vec3d p2_master = pose2_master.K * (pose2_master.R * points3d[i] + pose2_master.t);
        p1_master /= p1_master[2];
        p2_master /= p2_master[2];

        math::Vec3d p1_slave = pose1_slave.K * (pose1_slave.R * points3d[i] + pose1_slave.t);
        math::Vec3d p2_slave = pose2_slave.K * (pose2_slave.R * points3d[i] + pose2_slave.t);

        p1_slave /= p1_slave[2];
        p2_slave /= p2_slave[2];

        sfm::Correspondence2D2D match;
        match.p1[0]=p1_master[0];
        match.p1[1]=p1_master[1];
        match.p2[0]=p1_slave[0];
        match.p2[1]=p1_slave[1];
        EXPECT_TRUE(sfm::is_consistent_pose(match, pose1_master, pose1_slave));

        for (int j = 0; j < 3; ++j)
        {

            points2d_v1_master(j, i) = p1_master[j];
            points2d_v2_master(j, i) = p2_master[j];

            points2d_v1_slave(j, i) = p1_slave[j];
            points2d_v2_slave(j, i) = p2_slave[j];


        }
    }


    for (unsigned int i = 0; i < points3d.size(); ++i)
    {
        double disp = -(points2d_v1_master(0, i)-points2d_v1_slave(0, i)) ;
        math::Vec3d xyd=sfm::bundler::stereo::cam_pt_to_rect_xyd( pose1_master.K,tx,points3d[i]);
        EXPECT_NEAR(disp, xyd[2], 1e-6);
        EXPECT_NEAR(points2d_v1_master(0, i) , xyd[0], 1e-6);
        EXPECT_NEAR(points2d_v1_master(1, i) , xyd[1], 1e-6);

        EXPECT_NEAR(points2d_v1_slave(0, i) , xyd[0] + disp, 1e-6);
        EXPECT_NEAR(points2d_v1_slave(1, i) , xyd[1], 1e-6);


    }



    for (unsigned int i = 0; i < points3d.size(); ++i)
    {
        std::cout << points2d_v2_master(0, i)<< " "<<points2d_v2_slave(0, i)  <<std::endl;
        double disp = -(points2d_v2_master(0, i)-points2d_v2_slave(0, i)) ;
        math::Vec3d xyd=sfm::bundler::stereo::cam_pt_to_rect_xyd( pose2_master.K,tx,gt_pose*math::Vec4d(points3d[i],1.0));
        EXPECT_NEAR(disp, xyd[2], 1e-6);
        EXPECT_NEAR(points2d_v2_master(0, i) , xyd[0], 1e-6);
        EXPECT_NEAR(points2d_v2_master(1, i) , xyd[1], 1e-6);

        EXPECT_NEAR(points2d_v2_slave(0, i) , xyd[0] + disp, 1e-6);
        EXPECT_NEAR(points2d_v2_slave(1, i) , xyd[1], 1e-6);

    }


    sfm::bundler::stereo::StereoMatchCorrespondences2D3D unfiltered_matches;

    for (unsigned int i = 0; i < points3d.size(); ++i)
    {
        math::Vec3d points3d_trans=gt_pose*math::Vec4d(points3d[i],1.0);
        sfm::bundler::stereo::StereoMatchCorrespondence2D3D c;
        for (int j = 0; j < 3; ++j)
        {

            c.sc1.p3d[j] = points3d[i][j];
            c.sc2.p3d[j] = points3d_trans[j];
        }
        for (int j = 0; j < 2; ++j)
        {


            c.sc1.p2d[j] = points2d_v1_master(j, i) ;
            c.sc1.p2d_slave[j]= points2d_v1_slave(j, i) ;


            c.sc2.p2d[j] = points2d_v2_master(j, i) ;
            c.sc2.p2d_slave[j]= points2d_v2_slave(j, i) ;
        }
        unfiltered_matches.push_back(c);

    }

    int num_garbage_points=5;
    for (int i = 0; i < num_garbage_points; ++i)
    {
        math::Vec3d points3d_trans=gt_pose*math::Vec4d(rand() % points3d.size(),1.0);
        sfm::bundler::stereo::StereoMatchCorrespondence2D3D c;
        for (int j = 0; j < 3; ++j)
        {

            c.sc1.p3d[j] = points3d[rand() % points3d.size()][j];
            c.sc2.p3d[j] = points3d_trans[j];
        }


        c.sc1.p2d[0] = rand() % 800;
        c.sc1.p2d_slave[0]= c.sc1.p2d[0] + (rand() % 50);

        c.sc1.p2d[1] = rand() % 600;
        c.sc1.p2d_slave[1]= c.sc1.p2d[1] + (rand() % 2);


        c.sc2.p2d[0] = rand() % 800;
        c.sc2.p2d_slave[0]= c.sc2.p2d[0] + (rand() % 50);

        c.sc2.p2d[1] = rand() % 600;
        c.sc2.p2d_slave[1]= c.sc2.p2d[1] + (rand() % 2);

        unfiltered_matches.push_back(c);

    }
    math::Matrix<double, 3, 4>  result;

    stereo::pose_stereo_rigid(points3d[0], points3d[1], points3d[2],
            points3d_frame2[0], points3d_frame2[1], points3d_frame2[2], result);
    EXPECT_TRUE(result.is_similar(gt_pose, 1e-10));
    std::cout << result <<std::endl;
    std::cout << gt_pose <<std::endl;

    /* Compute pose from 3D correspondences using RANSAC. */
   sfm::bundler::stereo::RansacPoseRigidStereo::Result ransac_result;
   sfm::bundler::stereo::RansacPoseRigidStereo::Options opts;

    {
        sfm::bundler::stereo::RansacPoseRigidStereo ransac(opts);
        ransac.estimate(unfiltered_matches, pose1_master.K, tx, &ransac_result);
    //   std::cout << gt_pose <<"\n" << ransac_result.pose<<"\n";
        bool found_good_solution =ransac_result.pose.is_similar(gt_pose, 1e-10);
        EXPECT_TRUE(found_good_solution);

    }

}
