/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 *
 * App to create MVE scenes from images and bundles. The app supports:
 * - Import of calibrated images from Photosynther and Noah bundler
 * - Import of calibrated images from VisualSfM
 * - Import of uncalibrated 8 bit, 16 bit or float images from a directory
 *   8 bit formats: JPEG, PNG, TIFF, PPM
 *   16 bit formats: TIFF, PPM
 *   float formats: PFM
 */

#include <cstring>
#include <cstdlib>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>

#include "math/matrix.h"
#include "math/matrix_tools.h"
#include "math/algo.h"
#include "util/system.h"
#include "util/timer.h"
#include "util/arguments.h"
#include "util/string.h"
#include "util/file_system.h"
#include "util/tokenizer.h"
#include "mve/bundle.h"
#include "mve/bundle_io.h"
#include "mve/view.h"
#include "mve/image.h"
#include "mve/image_tools.h"
#include "mve/image_io.h"
#include "mve/image_exif.h"
#include "mve/mesh.h"
#include "mve/mesh_tools.h"

#include "stereo/stereo_matching.h"
#include "stereo/stereo_common.h"
#include "stereo/stereo_features.h"
#include "stereo/stereo_filename_utils.h"

#define THUMBNAIL_SIZE 50

#define PS_BUNDLE_LOG "coll.log"
#define PS_IMAGE_DIR "images/"
#define PS_UNDIST_DIR "undistorted/"
#define BUNDLER_FILE_LIST "list.txt"
#define BUNDLER_IMAGE_DIR ""
#define VIEWS_DIR "views/"

typedef std::vector<std::string> StringVector;
typedef std::vector<util::fs::File> FileVector;
typedef FileVector::iterator FileIterator;

/* ---------------------------------------------------------------- */

struct AppSettings
{
    std::string input_path;
    std::string output_path;
    std::string stereo_calib;

    int bundle_id;
    bool import_orig;
    bool skip_invalid;
    bool images_only;
    bool append_images;
    int max_pixels;
    int trim_name;
    std::string master_substr;
    std::string slave_substr;

    /* Computed values. */
    std::string bundle_path;
    std::string views_path;
};

/* ---------------------------------------------------------------- */

void
wait_for_user_confirmation (void)
{
    std::cerr << "-> Press ENTER to continue, or CTRL-C to exit." << std::endl;
    std::string line;
    std::getline(std::cin, line);
}

/* ---------------------------------------------------------------- */

void
read_noah_imagelist (std::string const& filename, StringVector& files)
{
    /*
     * The list of the original images is read from the list.txt file.
     */
    std::ifstream in(filename.c_str());
    if (!in.good())
    {
        std::cerr << "Error: Cannot read bundler list file!" << std::endl;
        std::cerr << "File: " << filename << std::endl;
        std::exit(EXIT_FAILURE);
    }

    while (true)
    {
        std::string file, dummy;
        in >> file;
        std::getline(in, dummy);
        if (file.empty())
            break;
        files.push_back(file);
    }

    in.close();
}

/* ---------------------------------------------------------------- */

mve::ByteImage::Ptr
load_8bit_image (std::string const& fname, std::string* exif)
{
    std::string lcfname(util::string::lowercase(fname));
    std::string ext4 = util::string::right(lcfname, 4);
    std::string ext5 = util::string::right(lcfname, 5);
    try
    {
        if (ext4 == ".jpg" || ext5 == ".jpeg")
            return mve::image::load_jpg_file(fname, exif);
        else if (ext4 == ".png" ||  ext4 == ".ppm"
            || ext4 == ".tif" || ext5 == ".tiff")
            return mve::image::load_file(fname);
    }
    catch (...)
    { }

    return mve::ByteImage::Ptr();
}

/* ---------------------------------------------------------------- */

mve::RawImage::Ptr
load_16bit_image (std::string const& fname)
{
    std::string lcfname(util::string::lowercase(fname));
    std::string ext4 = util::string::right(lcfname, 4);
    std::string ext5 = util::string::right(lcfname, 5);
    try
    {
        if (ext4 == ".tif" || ext5 == ".tiff")
            return mve::image::load_tiff_16_file(fname);
        else if (ext4 == ".ppm")
            return mve::image::load_ppm_16_file(fname);
    }
    catch (...)
    { }

    return mve::RawImage::Ptr();
}

/* ---------------------------------------------------------------- */

mve::FloatImage::Ptr
load_float_image (std::string const& fname)
{
    std::string lcfname(util::string::lowercase(fname));
    std::string ext4 = util::string::right(lcfname, 4);
    try
    {
        if (ext4 == ".pfm")
            return mve::image::load_pfm_file(fname);
    }
    catch (...)
    { }

    return mve::FloatImage::Ptr();
}

/* ---------------------------------------------------------------- */

mve::ImageBase::Ptr
load_any_image (std::string const& fname, std::string* exif)
{
    mve::ByteImage::Ptr img_8 = load_8bit_image(fname, exif);
    if (img_8 != NULL)
        return img_8;

    mve::RawImage::Ptr img_16 = load_16bit_image(fname);
    if (img_16 != NULL)
        return img_16;

    mve::FloatImage::Ptr img_float = load_float_image(fname);
    if (img_float != NULL)
        return img_float;

    std::cerr << "Skipping file " << util::fs::basename(fname)
        << ", cannot load image." << std::endl;
    return mve::ImageBase::Ptr();
}

/* ---------------------------------------------------------------- */

template <typename T>
void
find_min_max_percentile (typename mve::Image<T>::ConstPtr image,
    T* vmin, T* vmax)
{
    typename mve::Image<T>::Ptr copy = mve::Image<T>::create(*image);
    std::sort(copy->begin(), copy->end());
    *vmin = copy->at(copy->get_value_amount() / 10);
    *vmax = copy->at(9 * copy->get_value_amount() / 10);
}

/* ---------------------------------------------------------------- */

void
add_exif_to_view (mve::View::Ptr view, std::string const& exif)
{
    if (exif.empty())
        return;

    mve::ByteImage::Ptr exif_image = mve::ByteImage::create(exif.size(), 1, 1);
    std::copy(exif.begin(), exif.end(), exif_image->begin());
    view->set_blob(exif_image, "exif");
}

/* ---------------------------------------------------------------- */

mve::ByteImage::Ptr
create_thumbnail (mve::ImageBase::ConstPtr img)
{
    mve::ByteImage::Ptr image;
    switch (img->get_type())
    {
        case mve::IMAGE_TYPE_UINT8:
            image = mve::image::create_thumbnail<uint8_t>
                (std::dynamic_pointer_cast<mve::ByteImage const>(img),
                THUMBNAIL_SIZE, THUMBNAIL_SIZE);
            break;

        case mve::IMAGE_TYPE_UINT16:
        {
            mve::RawImage::Ptr temp = mve::image::create_thumbnail<uint16_t>
                (std::dynamic_pointer_cast<mve::RawImage const>(img),
                THUMBNAIL_SIZE, THUMBNAIL_SIZE);
            uint16_t vmin, vmax;
            find_min_max_percentile(temp, &vmin, &vmax);
            image = mve::image::raw_to_byte_image(temp, vmin, vmax);
            break;
        }

        case mve::IMAGE_TYPE_FLOAT:
        {
            mve::FloatImage::Ptr temp = mve::image::create_thumbnail<float>
                (std::dynamic_pointer_cast<mve::FloatImage const>(img),
                THUMBNAIL_SIZE, THUMBNAIL_SIZE);
            float vmin, vmax;
            find_min_max_percentile(temp, &vmin, &vmax);
            image = mve::image::float_to_byte_image(temp, vmin, vmax);
            break;
        }

        default:
            return mve::ByteImage::Ptr();
    }

    return image;
}

/* ---------------------------------------------------------------- */

std::string
remove_file_extension (std::string const& filename)
{
    std::size_t pos = filename.find_last_of('.');
    if (pos != std::string::npos)
        return filename.substr(0, pos);
    return filename;
}

/* ---------------------------------------------------------------- */

template <class T>
typename mve::Image<T>::Ptr
limit_image_size (typename mve::Image<T>::Ptr img, int max_pixels)
{
    while (img->get_pixel_amount() > max_pixels)
        img = mve::image::rescale_half_size<T>(img);
    return img;
}

/* ---------------------------------------------------------------- */

mve::ImageBase::Ptr
limit_image_size (mve::ImageBase::Ptr image, int max_pixels)
{
    switch (image->get_type())
    {
        case mve::IMAGE_TYPE_FLOAT:
            return limit_image_size<float>(std::dynamic_pointer_cast
                <mve::FloatImage>(image), max_pixels);
        case mve::IMAGE_TYPE_UINT8:
            return limit_image_size<uint8_t>(std::dynamic_pointer_cast
                <mve::ByteImage>(image), max_pixels);
        case mve::IMAGE_TYPE_UINT16:
            return limit_image_size<uint16_t>(std::dynamic_pointer_cast
                <mve::RawImage>(image), max_pixels);
        default:
            break;
    }
    return mve::ImageBase::Ptr();
}

/* ---------------------------------------------------------------- */

bool
has_jpeg_extension (std::string const& filename)
{
    std::string lcfname(util::string::lowercase(filename));
    return util::string::right(lcfname, 4) == ".jpg"
        || util::string::right(lcfname, 5) == ".jpeg";
}

/* ---------------------------------------------------------------- */

std::string
make_image_name (int id)
{
    return "view_" + util::string::get_filled(id, 4) + ".mve";
}

/* ---------------------------------------------------------------- */

int
find_max_scene_id (std::string const& view_path)
{
    util::fs::Directory dir;
    try { dir.scan(view_path); }
    catch (...) { return -1; }

    /* Load all MVE files and remember largest view ID. */
    int max_view_id = 0;
    for (std::size_t i = 0; i < dir.size(); ++i)
    {
        std::string ext4 = util::string::right(dir[i].name, 4);
        if (ext4 != ".mve")
            continue;

        mve::View::Ptr view;
        try
        { view = mve::View::create(dir[i].get_absolute_name()); }
        catch (...)
        {
            std::cerr << "Error reading " << dir[i].name << std::endl;
            continue;
        }

        max_view_id = std::max(max_view_id, view->get_id());
    }

    return max_view_id;
}

/* ---------------------------------------------------------------- */

void
import_images (AppSettings const& conf)
{
    util::WallTimer timer;

    util::fs::Directory dir;
    try { dir.scan(conf.input_path); }
    catch (std::exception& e)
    {
        std::cerr << "Error scanning input dir: " << e.what() << std::endl;
        std::exit(EXIT_FAILURE);
    }
    std::cout << "Found " << dir.size() << " directory entries." << std::endl;

    /* ------------------ Start importing images ------------------- */

    /* Create destination dir. */
    if (!conf.append_images)
    {
        std::cout << "Creating output directories..." << std::endl;
        util::fs::mkdir(conf.output_path.c_str());
        util::fs::mkdir(conf.views_path.c_str());
    }

    int max_scene_id = -1;
    if (conf.append_images)
    {
        max_scene_id = find_max_scene_id(conf.views_path);
        if (max_scene_id < 0)
        {
            std::cerr << "Error: Cannot find view ID for appending." << std::endl;
            std::exit(EXIT_FAILURE);
        }
    }

    /* Sort file names, iterate over file names. */
    std::sort(dir.begin(), dir.end());
    util::fs::Directory master_dir,slave_dir;
    ExpandWildCard(dir,master_dir,conf.master_substr);
    ExpandWildCard(dir,slave_dir,conf.slave_substr);
    std::vector<std::pair<util::fs::File,util::fs::File> > pairs;
    RemoveNonPairs(master_dir,slave_dir,conf.master_substr,conf.slave_substr,conf.trim_name,pairs);

    int id_cnt = max_scene_id + 1;
    int num_imported = 0;
    for (auto p: pairs)
    {
        if (p.first.is_dir || p.second.is_dir)
        {
            std::cout << "Skipping directory " << p.first.name << std::endl;
            continue;
        }

        std::string fname_master = p.first.name;
        std::string afname_master = p.first.get_absolute_name();

        std::cout << "Importing image " << fname_master << "..." << std::endl;
        std::string exif;
        mve::ImageBase::Ptr image_master = load_any_image(afname_master, &exif);
        if (image_master == NULL)
            continue;


        std::string fname_slave = p.second.name;
        std::string afname_slave = p.second.get_absolute_name();

        mve::ImageBase::Ptr image_slave = load_any_image(afname_slave, &exif);
        if (image_slave == NULL)
            continue;
        /* Create view, set headers, add image. */
        mve::View::Ptr view = mve::View::create();
        view->set_id(id_cnt);
        view->set_name(remove_file_extension(fname_master));

        /* Rescale and add original image. */
        int orig_width = image_master->width();
        image_master = limit_image_size(image_master, conf.max_pixels);
        if (orig_width == image_master->width() && has_jpeg_extension(fname_master))
            view->set_image_ref(afname_master, "original");
        else
            view->set_image(image_master, "original");

        orig_width = image_slave->width();
        image_slave = limit_image_size(image_slave, conf.max_pixels);
        if (orig_width == image_slave->width() && has_jpeg_extension(fname_slave))
            view->set_image_ref(afname_slave, "slave");
        else
            view->set_image(image_slave, "slave");


        /* Add thumbnail for byte images. */
        mve::ByteImage::Ptr thumb = create_thumbnail(image_master);
        if (thumb != NULL)
            view->set_image(thumb, "thumbnail");

        /* Add EXIF data to view if available. */
        add_exif_to_view(view, exif);
        std::vector<char> calib_data;
        ReadAllBytes(conf.stereo_calib.c_str(),calib_data);
        if(calib_data.size() == 0){
            std::cout <<"Can't load stereo calib file:" <<conf.stereo_calib << std::endl;
            throw std::invalid_argument("Calib file invaid");
        }

        mve::ByteImage::Ptr calib_data_image = mve::ByteImage::create(calib_data.size(), 1, 1);
        std::copy(calib_data.begin(), calib_data.end(), calib_data_image->begin());
        view->set_blob(calib_data_image,  "stereocalib");

        /* Save view to disc. */
        std::string mve_fname = make_image_name(id_cnt);
        std::cout << "Writing MVE view: " << mve_fname << "..." << std::endl;
        view->save_view_as(util::fs::join_path(conf.views_path, mve_fname));

        /* Advance ID of successfully imported images. */
        id_cnt += 1;
        num_imported += 1;
    }

    std::cout << "Imported " << num_imported << " input images, "
        << "took " << timer.get_elapsed() << " ms." << std::endl;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
/* ---------------------------------------------------------------- */

int
main (int argc, char** argv)
{
    util::system::register_segfault_handler();
    util::system::print_build_timestamp("MVE Makescene");

    /* Setup argument parser. */
    util::Arguments args;
    args.set_usage(argv[0], "[ OPTIONS ] INPUT OUT_SCENE STEREO_CALIB");
    args.set_exit_on_error(true);
    args.set_nonopt_maxnum(3);
    args.set_nonopt_minnum(3);
    args.set_helptext_indent(22);
    args.set_description("This utility creates MVE scenes from images "
        "With the \"images-only\" option, all images in the INPUT directory "
        "are imported without camera information. If \"append-images\" is "
        "specified, images are added to an existing scene.");
    args.add_option('o', "original", false, "Import original images");
    args.add_option('k', "keep-invalid", false, "Keeps images with invalid cameras");
    args.add_option('i', "images-only", true, "Imports images from INPUT_DIR using <mastersubstr,slavesubstr> (e.g. LC,RM)");
    args.add_option('t', "trim-name", true, "Trim end of names (for poorly associated timestamps)");
    args.add_option('a', "append-images", false, "Appends images to an existing scene");
    args.add_option('m', "max-pixels", true, "Limit image size by iterative half-sizing");
    args.parse(argc, argv);

    /* Setup defaults. */
    AppSettings conf;
    conf.import_orig = false;
    conf.skip_invalid = true;
    conf.images_only = false;
    conf.append_images = false;
    conf.input_path = util::fs::sanitize_path(args.get_nth_nonopt(0));
    conf.output_path = util::fs::sanitize_path(args.get_nth_nonopt(1));
    conf.stereo_calib = util::fs::sanitize_path(args.get_nth_nonopt(2));

    conf.bundle_id = 0;
    conf.max_pixels = std::numeric_limits<int>::max();
    conf.trim_name = 0;

    /* General settings. */
    for (util::ArgResult const* i = args.next_option();
        i != NULL; i = args.next_option())
    {

        if (i->opt->lopt == "keep-invalid")
            conf.skip_invalid = false;
        else if (i->opt->lopt == "images-only"){
            conf.images_only = true;
            auto exts=split(i->get_arg<std::string>(),',');
            if(exts.size() == 2){
                conf.master_substr = exts[0];
                conf.slave_substr = exts[1];
            }else
                throw std::invalid_argument("Images master slave substrings incorrectly passed");

        }
        else if (i->opt->lopt == "append-images")
            conf.append_images = true;
        else if (i->opt->lopt == "trim-name")
            conf.trim_name = i->get_arg<int>();
        else if (i->opt->lopt == "max-pixels")
            conf.max_pixels = i->get_arg<int>();
        else
            throw std::invalid_argument("Unexpected option");
    }

    /* Check command line arguments. */
    if (conf.input_path.empty() || conf.output_path.empty())
    {
        args.generate_helptext(std::cerr);
        return EXIT_FAILURE;
    }
    conf.views_path = util::fs::join_path(conf.output_path, VIEWS_DIR);

    if (conf.append_images && !conf.images_only)
    {
        std::cerr << "Error: Cannot --append-images without --images-only."
            << std::endl;
        return EXIT_FAILURE;
    }

    /* Check if output dir exists. */
    bool output_path_exists = util::fs::dir_exists(conf.output_path.c_str());
    if (output_path_exists && !conf.append_images)
    {
        std::cerr << std::endl;
        std::cerr << "** Warning: Output dir already exists." << std::endl;
        std::cerr << "** This may leave old views in your scene." << std::endl;
        wait_for_user_confirmation();
    }
    else if (!output_path_exists && conf.append_images)
    {
        std::cerr << "Error: Output dir does not exist. Cannot append images."
            << std::endl;
        return EXIT_FAILURE;
    }

    if (conf.images_only)
        import_images(conf);


    return EXIT_SUCCESS;
}
